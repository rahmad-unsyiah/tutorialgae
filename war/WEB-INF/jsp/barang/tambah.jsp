<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="UTF-8">
        <title>Tambah Barang</title>
    </head>
    <body>
        <h1>Tambah Barang</h1>
        <form id="frmTambahBarang" method="POST" action="/barang/tambah">
            Kode: <input id="txtKode" name="txtKode" type="text"/><br/>
            Nama: <input id="txtNama" name="txtNama" type="text"/><br/>
            Keterangan: <textarea id="txtKeterangan" name="txtKeterangan" cols="40" rows="5"></textarea><br/><br/>
            <input id="btnSubmit" name="btnSubmit" type="submit" value="Tambah">
            &nbsp;
            &nbsp;
            <a href="/barang"><input id="btnCancel" type="button" value="Batal"></a>
        </form>
    </body>
</html>