<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Daftar Barang</title>
    </head>
    <body>
        <h1>Daftar Barang</h1>
        <a href="/barang/tambah"><input id="btnTambahBarang" type="button" value="Tambah Barang"/></a>
        <br/>
        <table id="tblDaftarBarang" border="1">
            <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th>Keterangan</th>
                <th>&nbsp;</th>
            </tr>
<c:forEach var="satuBarang" items="${daftarBarang}">
            <tr>
                <td>${satuBarang.kode}</td>
                <td>${satuBarang.nama}</td>
                <td>${satuBarang.keterangan}</td>
                <td>
                    <a href="/barang/ubah?id=${satuBarang.key.id}"><input id="btnUbah" type="button" value="Ubah"/></a>
                    <a href="/barang/hapus?id=${satuBarang.key.id}"><input id="btnHapus" type="submit" value="Hapus"/></a>
                </td>
            </tr>
</c:forEach>
        </table>
    </body>
</html>