<!DOCTYPE html>
<html lang="id">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Hapus Barang</title>
    </head>
    <body>
        <h1>Hapus Barang</h1>
        <form id="frmHapusBarang" method="POST" action="/barang/hapus">
            <input type="hidden" id="hdnId" name="hdnId" value="${barang.key.id}">
            Kode: <input id="txtKode" type="text" name="txtKode" value="${barang.kode}" disabled/><br/>
            Nama: <input id="txtNama" type="text" name="txtNama" value="${barang.nama}" disabled/><br/>
            Keterangan: <textarea id="txtKeterangan" name="txtKeterangan" cols="40" rows="5" disabled>${barang.keterangan}</textarea><br/><br/>
            <input id="btnSubmit" name="btnSubmit" type="submit" value="Hapus">
            &nbsp;
            &nbsp;
            <a href="/barang"><input type="button" id="btnCancel" value="Batal"></a>
        </form>
    </body>
</html>