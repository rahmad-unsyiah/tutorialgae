<!DOCTYPE html>
<html lang="id">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Ubah Barang</title>
    </head>
    <body>
        <h1>Ubah Barang</h1>
        <form id="frmUbahBarang" method="POST" action="/barang/ubah">
            <input id="hdnId" type="hidden" name="hdnId" value="${barang.key.id}">
            Kode: <input id="txtKode" type="text" name="txtKode" value="${barang.kode}"/><br/>
            Nama: <input id="txtNama" type="text" name="txtNama" value="${barang.nama}"/><br/>
            Keterangan: <textarea id="txtKeterangan" name="txtKeterangan" cols="40" rows="5">${barang.keterangan}</textarea><br/><br/>
            <input id="btnSubmit" name="btnSubmit" type="submit" value="Ubah">
            &nbsp;
            &nbsp;
            <a href="/barang"><input id="btnCancel" type="button" value="Batal"></a>
        </form>
    </body>
</html>