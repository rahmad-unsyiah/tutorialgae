<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <Title>Utama - Admin</Title>
    </head>
    <body>
        <h1>Halaman Utama - Admin</h1>
        <a href="/admin/tambah"><input id="btnTambahAdmin" type="button" value="Tambah Admin"/></a>
        &nbsp;&nbsp;&nbsp;
        <a href="/admin/logout">Logout</a>
        <br/>
        <h3>Daftar Pemakai</h3>
        <table id="tblDaftarPemakai" border="1">
            <tr>
                <th>Username</th>
                <th>Peran</th>
                <th>&nbsp;</th>
            </tr>
<c:forEach var="satuPemakai" items="${daftarPemakai}">
            <tr>
                <td>${satuPemakai.username}</td>
                <td>
                    <c:choose>
                        <c:when test="${satuPemakai.peran == 1}">Admin</c:when>
                        <c:when test="${satuPemakai.peran == 2}">Bukan Admin</c:when>
                        <c:otherwise>Tidak dikenal...</c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <a href="/admin/ubah?id=${satuPemakai.key.id}"><input id="btnUbah" type="button" value="Ubah"/></a>
                    <a href="/admin/hapus?id=${satuPemakai.key.id}"><input id="btnHapus" type="submit" value="Hapus"/></a>
                </td>
            </tr>
</c:forEach>
        </table>
    </body>
</html>