<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="UTF-8">
        <title>Tambah Pemakai</title>
    </head>
    <body>
        <h1>Tambah Pemakai</h1>
        <form id="frmTambahPemakai" method="POST" action="/admin/tambah">
            Username: <input id="txtUsername" name="txtUsername" type="text" required/><br/>
            Password: <input id="passPassword" name="passPassword" type="password" required/><br/>
            Peran: 
            <select id="selPeran" name="selPeran" required>
                <option value="1">Admin</option>
                <option value="2" selected>Bukan Admin</option>
            </select>
            <br/><br/>
            <input id="btnSubmit" name="btnSubmit" type="submit" value="Tambah">
            &nbsp;
            &nbsp;
            <a href="/admin"><input id="btnCancel" type="button" value="Batal"></a>
        </form>
    </body>
</html>