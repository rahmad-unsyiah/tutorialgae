<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Hapus Pemakai</title>
    </head>
    <body>
        <h1>Hapus Pemakai</h1>
        <form id="frmHapusPemakai" method="POST" action="/admin/hapus">
            <input id="hdnId" type="hidden" name="hdnId" value="${pemakai.key.id}">
            Username: <input id="txtUsername" type="text" name="txtUsername" value="${pemakai.username}" disabled/><br/>
            Password: <input id="passPassword" type="password" name="passPassword" value=" " disabled/><br/>
            Peran:
            <select name="selPeran" id="selPeran" disabled>
<c:choose>
    <c:when test="${pemakai.peran == 1}">
                <option value="1" selected>Admin</option>
                <option value="2">Bukan Admin</option>
    </c:when>
    <c:when test="${pemakai.peran == 2}">
                <option value="1">Admin</option>
                <option value="2" selected>Bukan Admin</option>
    </c:when>
    <c:otherwise>
                <option value="1">Admin</option>
                <option value="2">Bukan Admin</option>
    </c:otherwise>
</c:choose>
            </select> 
            <br/><br/>
            <input id="btnSubmit" name="btnSubmit" type="submit" value="Hapus">
            &nbsp;
            &nbsp;
            <a href="/admin"><input id="btnCancel" type="button" value="Batal"></a>
        </form>
    </body>
</html>