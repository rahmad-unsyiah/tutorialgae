<!DOCTYPE html>
<html lang="id">
    <head>
        <Title>Login - Admin</Title>
    </head>
    <body>
        <h1>Login</h1>
        <form method="post" action="/admin/login">
            Username: <input type="text" name="txtUsername" required><br/>
            Password: <input type="password" name="passPassword" required><br/>
            <input type="submit" name="btnLogin" value="Login">
            <br/>
            <a href="${urlLoginGoogleAdmin}">Login Admin Google</a>
        </form>
    </body>
</html>