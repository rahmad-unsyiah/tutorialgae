package id.ac.unsyiah.trc.servlet.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import id.ac.unsyiah.trc.model.pemakai.AturPemakai;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Servlet untuk tampilkan halaman utama admin.
 */
public class Utama  extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman utama admin.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) 
				throws ServletException, IOException {
		if (loginSebagaiAdmin(req) == false) {
			resp.sendRedirect("/admin/login");
			return;
		}
		
		// Ambil daftar pemakai dari Datastore
		List<Pemakai> daftarPemakai = new AturPemakai().daftar();
		
		// Kirim daftar barang ke halaman JSP untuk ditampilkan
		req.setAttribute("daftarPemakai", daftarPemakai);

		// Panggil utama.jsp punya admin
		resp.setContentType("text/html");
		RequestDispatcher jsp = 
				req.getRequestDispatcher("/WEB-INF/jsp/admin/utama.jsp");
		jsp.forward(req, resp);		
	}
	
	/**
	 * Periksa apakah seorang Pemakai telah login dan antara admin atau google 
	 * admin.
	 *  
	 * @return True jika si Pemakai telah login dan antara admin atau google dan
	 *         False selain itu.
	 */
	private boolean loginSebagaiAdmin(HttpServletRequest req) {
		// Periksa apakah admin google 
		UserService userService = UserServiceFactory.getUserService();
		if (userService != null) // Apakah telah login via Google
			if (userService.isUserLoggedIn() == true) // Masih login
				if (userService.isUserAdmin() == true) // Yang login admin 
													   // google
					return true;
		
		// Bukan admin google, periksa apakan Pemakai dengan peran Admin
		HttpSession session = req.getSession(false);
		if (session != null) { // Jika tidak ada session sama sekali berarti 
							   // belum login
			Pemakai pemakai = (Pemakai) session.getAttribute("pemakai");
			if (pemakai != null) { // Session object pemakai tidak ada berarti 
								   // belum login
				if (pemakai.getKey() != null) { // Jika session object pemakai 
												// tidak ada key berarti belum 
												// di datastore maka statusnya 
												// belum login
					if (pemakai.getPeran() == Pemakai.PERAN_ADMIN) // Perannya
																   // Admin
						return true;
				}
			}				
		}
				
		return false;
	}

	private static final long serialVersionUID = -7850083523490887728L;
}