package id.ac.unsyiah.trc.servlet.admin;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.UserServiceFactory;

import id.ac.unsyiah.trc.eksepsi.LoginSalah;
import id.ac.unsyiah.trc.model.pemakai.AturPemakai;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Servlet untuk tangain proses Login oleh Admin
 */
public class Login  extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman login untuk Admin.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) 
				throws ServletException, IOException {
		// Bangkitkan URL untuk login sebagai Admin Google
		String urlLoginGoogleAdmin = 
				UserServiceFactory.getUserService().createLoginURL("/admin");
		// Kirim link ke halaman
		req.setAttribute("urlLoginGoogleAdmin", urlLoginGoogleAdmin);
		
		// Panggil login.jsp punya admin
		resp.setContentType("text/html");
		RequestDispatcher jsp = 
				req.getRequestDispatcher("/WEB-INF/jsp/admin/login.jsp");
		jsp.forward(req, resp);		
	}
	
	/**
	 * Tangani HTTP POST. Dalam hal ini proses login untuk Admin.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp) 
				throws ServletException, IOException {
		// Ambil data yang diisi
        String username = req.getParameter("txtUsername");
        String password = req.getParameter("passPassword");
        
        Pemakai pemakai = null;
        try {
        	// Periksa login benar apa tidak
        	pemakai = new AturPemakai().periksaLogin(username, password);
        	        	
	        // Login benar, simpan informasi Pemakai sebagai session untuk 
        	// dipakai dikemudian
	        HttpSession session = req.getSession(true);
	        session.setAttribute("pemakai", pemakai);
	        // Pastikan lama tidak aktif sebelum dipaksa logout
	        session.setMaxInactiveInterval(60 * 60 * 8); // 8 jam dalam detik
        	
	        // Kirim ke halaman utama admin
            resp.sendRedirect("/admin");
        }
        catch(LoginSalah salah) {
	        // Terjadi kesalahan login kirim kembali ke halaman login admin
	        resp.sendRedirect("/admin/login");
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException salah) {
			salah.printStackTrace();
	        // Terjadi kesalahan login kirim kembali ke halaman login admin
	        resp.sendRedirect("/admin/login");
		}        
	}

	private static final long serialVersionUID = 5650372384404682014L;
}