package id.ac.unsyiah.trc.servlet.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

/**
 * Servlet untuk melakukan proses logout
 */
public class Logout  extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman utama admin.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) 
				throws ServletException, IOException {
		// Jika login sebagai admin, logout pakai google logout
		UserService userService = UserServiceFactory.getUserService();
		if (userService != null) // Apakah telah login via Google
			if (userService.isUserLoggedIn() == true) // Masih login
				if (userService.isUserAdmin() == true) { // Yang login admin 
													     // google
					String logoutURL = 
							UserServiceFactory.getUserService()
										      .createLogoutURL("/admin/login");
			
					// Alihkan ke halaman logout Google
					resp.sendRedirect(logoutURL);
					return;
				}

		// Jika login sebagai user bisa, hapus semua session
		HttpSession session = req.getSession(false);
		if (session != null)
			session.invalidate();

		// Alihkan ke halaman login admin
		resp.sendRedirect("/admin/login");
	}

	private static final long serialVersionUID = -1140730563044335419L;
}