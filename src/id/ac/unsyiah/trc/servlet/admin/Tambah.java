package id.ac.unsyiah.trc.servlet.admin;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.ac.unsyiah.trc.eksepsi.DuplikasiUsername;
import id.ac.unsyiah.trc.model.pemakai.AturPemakai;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

public class Tambah extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman tambah {@link Pemakai}.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
		// Panggil tambah.jsp
		resp.setContentType("text/html");
		RequestDispatcher jsp = 
				req.getRequestDispatcher("/WEB-INF/jsp/admin/tambah.jsp");
		jsp.forward(req, resp);		
	}
	
	/**
	 * Tangani HTTP POST. Dalam hal ini tanganin pengiriman form untuk menambah
	 * {@link Pemakai} baru.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
        // Mengambil inputan dari form
        String username = req.getParameter("txtUsername");
        String password = req.getParameter("passPassword");
        int peran = Integer.valueOf(req.getParameter("selPeran"));
        
        // Tambah data ke datastore
        try {
			new AturPemakai().baru(username, password, peran);
		} 
        catch (DuplikasiUsername salah) {
        	// Username telah dipakai
			salah.printStackTrace();
		} 
        catch (NoSuchAlgorithmException | InvalidKeySpecException salah) {
			salah.printStackTrace();
		}
        
        // Kirim kembali ke halaman daftar barang
        resp.sendRedirect("/admin");
	}

	private static final long serialVersionUID = -5507736613790252868L;
}