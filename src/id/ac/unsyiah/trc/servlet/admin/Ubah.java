package id.ac.unsyiah.trc.servlet.admin;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.ac.unsyiah.trc.model.pemakai.AturPemakai;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

public class Ubah extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman ubah {@link Pemakai}.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
        // Ambil ID pemakai yang mau diubah 
        String idStr = req.getParameter("id");
        long id = Long.valueOf(idStr);
        
        // Ambil data dari datastore
        Pemakai pemakai = new AturPemakai().cari(id);

		// Kirim barang yang mau diubah ke halaman JSP untuk ditampilkan
		req.setAttribute("pemakai", pemakai);

		// Panggil ubah.jsp
		resp.setContentType("text/html");
		RequestDispatcher jsp = 
				req.getRequestDispatcher("/WEB-INF/jsp/admin/ubah.jsp");
		jsp.forward(req, resp);		
	}
	
	/**
	 * Tangani HTTP POST. Dalam hal ini tanganin pengiriman form untuk mengubah
	 * {@link Pemakai}.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
        // Mengambil inputan dari form
		String idStr = req.getParameter("hdnId");
		Long id = Long.valueOf(idStr);
        String password = req.getParameter("passPassword");
        int peran = Integer.valueOf(req.getParameter("selPeran"));
        
        // Ubah data di datastore
        try {
			new AturPemakai().ubahPeran(id, peran);
			new AturPemakai().ubahPassword(id, password);
		} 
		catch (NoSuchAlgorithmException | InvalidKeySpecException salah) {
			salah.printStackTrace();
		}
        
        // Kirim kembali ke halaman daftar barang
        resp.sendRedirect("/admin");
	}

	private static final long serialVersionUID = -2789291647500005527L;
}