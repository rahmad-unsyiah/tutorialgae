package id.ac.unsyiah.trc.servlet.barang;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.ac.unsyiah.trc.model.barang.AturBarang;
import id.ac.unsyiah.trc.model.barang.Barang;

public class Tambah extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman tambah {@link Barang}.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
		// Panggil tambah.jsp
		resp.setContentType("text/html");
		RequestDispatcher jsp = 
				req.getRequestDispatcher("/WEB-INF/jsp/barang/tambah.jsp");
		jsp.forward(req, resp);		
	}
	
	/**
	 * Tangani HTTP POST. Dalam hal ini tanganin pengiriman form untuk menambah
	 * {@link Barang} baru.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
        // Mengambil inputan dari form
        String kode = req.getParameter("txtKode"); // Perhatikan namanya sesuai 
        										   // yang di form
        String nama = req.getParameter("txtNama");
        String keterangan = req.getParameter("txtKeterangan");
        
        // Tambah data ke datastore
        new AturBarang().baru(kode, nama, keterangan);
        
        // Kirim kembali ke halaman daftar barang
        resp.sendRedirect("/barang");
	}

	private static final long serialVersionUID = 6703040623134950516L;
}