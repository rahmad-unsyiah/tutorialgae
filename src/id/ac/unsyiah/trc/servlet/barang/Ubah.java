package id.ac.unsyiah.trc.servlet.barang;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.ac.unsyiah.trc.model.barang.AturBarang;
import id.ac.unsyiah.trc.model.barang.Barang;

public class Ubah extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman tambah {@link Barang}.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
        // Ambil ID barang yang mau diubah 
        String idStr = req.getParameter("id");
        long id = Long.valueOf(idStr);
        
        // Ambil data dari datastore
        Barang barang = new AturBarang().cari(id);

		// Kirim barang yang mau diubah ke halaman JSP untuk ditampilkan
		req.setAttribute("barang", barang);

		// Panggil ubah.jsp
		resp.setContentType("text/html");
		RequestDispatcher jsp = 
				req.getRequestDispatcher("/WEB-INF/jsp/barang/ubah.jsp");
		jsp.forward(req, resp);		
	}
	
	/**
	 * Tangani HTTP POST. Dalam hal ini tanganin pengiriman form untuk menambah
	 * {@link Barang} baru.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
        // Mengambil inputan dari form
        String kode = req.getParameter("txtKode"); // Perhatikan namanya sesuai 
        										   // yang di form
        String nama = req.getParameter("txtNama");
        String keterangan = req.getParameter("txtKeterangan");
        String idStr = req.getParameter("hdnId");
        long id = Long.valueOf(idStr);
        
        // Ubah data di datastore
        new AturBarang().ubah(id, kode, nama, keterangan);
        
        // Kirim kembali ke halaman daftar barang
        resp.sendRedirect("/barang");
	}

	private static final long serialVersionUID = 6703040623134950516L;
}