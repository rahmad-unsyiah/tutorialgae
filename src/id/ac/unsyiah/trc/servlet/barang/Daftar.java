package id.ac.unsyiah.trc.servlet.barang;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.ac.unsyiah.trc.model.barang.AturBarang;
import id.ac.unsyiah.trc.model.barang.Barang;

public class Daftar extends HttpServlet {
	/**
	 * Tangani HTTP GET. Dalam hal ini tampilkan halaman daftar {@link Barang}.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {
		// Ambil daftar barang dari Datastore
		List<Barang> daftarBarang = new AturBarang().daftar();
		
		// Kirim daftar barang ke halaman JSP untuk ditampilkan
		req.setAttribute("daftarBarang", daftarBarang);
		
		// Panggil daftar.jsp
		resp.setContentType("text/html");
		RequestDispatcher jsp = 
				req.getRequestDispatcher("/WEB-INF/jsp/barang/daftar.jsp");
		jsp.forward(req, resp);		
	}	

	private static final long serialVersionUID = 6237617111023561784L;
}