/**
 * Controller ini berfungsi untuk mengakses model.Pemakai
 */
package id.ac.unsyiah.trc.model.pemakai;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import id.ac.unsyiah.trc.eksepsi.DuplikasiUsername;
import id.ac.unsyiah.trc.eksepsi.LoginSalah;
import id.ac.unsyiah.trc.model.PMF;
import id.ac.unsyiah.trc.tools.SecureHashWithSalt;

public class AturPemakai {
    /**
     * Buat pemakai baru dan simpan ke datastore
     * 
     * @param username Username dari pemakai baru
     * 
     * @return Pemakai yang baru dibuat. PERHATIKAN: Pemakai yang dikembalikan
     *         tidak bisa diubah karena PM-nya sudah ditutup!
     * 
     * @throws NoSuchAlgorithmException Jika algoritma untuk secure hash tidak 
     *                                  ada
     * @throws InvalidKeySpecException Jika spesifikasi enkripsi yang diminta 
     *                                 tidak sah
     * @throws DuplikasiUsername       Jika Username dari Pemakai baru telah ada
     */
    public Pemakai baru(String username, String password, long peran) 
                   throws NoSuchAlgorithmException,  
                   		  InvalidKeySpecException, 
                   		  DuplikasiUsername {
        // Cek apakah email telah ada sebelumnya
        if (cariUsername(username) != null)
            throw new DuplikasiUsername();
        
        // Secure-hash password-nya
        SecureHashWithSalt bangkitPassword = new SecureHashWithSalt();
        byte[] salt = bangkitPassword.buatSalt();
        byte[] passwordEnkripsi = bangkitPassword.enkripsi(password, salt);
        
        // Buat object dari pemakai baru 
        Pemakai pemakai = new Pemakai(username, passwordEnkripsi, salt, peran);
        
        // Simpan
        PersistenceManager pm = PMF.getInstance().getPersistenceManager();
        try { 
        	pm.makePersistent(pemakai); 
        } 
        finally { 
        	pm.close(); 
        }
        
        // Kembalikan pemakai baru
        // PERHATIKAN: Pemakai baru tidak bisa diubah karena PM-nya sudah 
        //             ditutup!
        return pemakai;
    }
    
    /**
     * Cari satu suatu Pemakai berdasarkan id-nya
     * 
     * @param id ID dari pemakai yang mau dicari
     * 
     * @return Pemakai yang dicari. PERHATIKAN: Pemakai yang dikembalikan
     *         tidak bisa diubah karena PM-nya sudah ditutup!
     * 
     * @exception javax.jdo.JDOObjectNotFoundException jika Pemakai dengan ID 
     *            yang diberikan tidak ditemukan
     */
    public Pemakai cari(long id) {
        // Buat key dari ID Pemakai 
        Key key = KeyFactory.createKey(Pemakai.class.getSimpleName(), id);
        
        // Ambil
        PersistenceManager pm = PMF.getInstance().getPersistenceManager();
        Pemakai pemakai = null;
        try { 
        	pemakai = pm.getObjectById(Pemakai.class, key); 
        }
        finally { 
        	pm.close(); 
        }
        
        return pemakai;
    }
    
    /**
     * Cari satu suatu Pemakai berdasarkan username-nya
     * 
     * @param usernameCari USername dari Pemakai yang hendak dicari
     * 
     * @return Pemakai yang dicari. PERHATIKAN: Pemakai yang dikembalikan
     *         tidak bisa diubah karena PM-nya sudah ditutup!
     * 
     * @exception javax.jdo.JDOObjectNotFoundException jika Pemakai dengan 
     * 			  username yang diberikan tidak ditemukan
     */
    @SuppressWarnings("unchecked")
    public Pemakai cariUsername(String usernameCari) {
        PersistenceManager pm = PMF.getInstance().getPersistenceManager();

        // Bangun query-nya
        Query query = pm.newQuery(Pemakai.class);
        query.setFilter("username == usernameCari");
        query.declareParameters("String usernameCari");

        // Baca
        List<Pemakai> daftar = null;
        Pemakai hasil = null;
        try { 
            // Cari
            daftar = (List<Pemakai>) query.execute(usernameCari); 
            // Pastikan hanya satu email yang ada, jika >1 ambil yg awal aja..
            if (daftar.size() >= 1)
                hasil = daftar.get(0);
        }
        finally { 
        	pm.close(); 
        }
        
        // Kembalikan hasilnya
        return hasil;
    }
    
    /**
     * Ubah password seorang Pemakai
     * 
     * @param id       ID dari Pemakai yang hendak diubah password-nya
     * @param password Password baru
     * 
     * @throws NoSuchAlgorithmException Jika algoritma untuk secure hash tidak 
     *                                  ada
     * @throws InvalidKeySpecException Jika spesifikasi enkripsi yang diminta 
     *                                 tidak sah
     */
    public void ubahPassword(Long id, String password) 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Buat key dari ID Pemakai 
        Key key = KeyFactory.createKey(Pemakai.class.getSimpleName(), id);
        
        // Ambil datanya
        PersistenceManager pm = PMF.getInstance().getPersistenceManager();
        Pemakai pemakai = null;
        try  { 
            // Cari
            pemakai = pm.getObjectById(Pemakai.class, key);
            // Ubah passwrod
            SecureHashWithSalt bangkitPassword = new SecureHashWithSalt();
            byte[] salt = bangkitPassword.buatSalt();
            byte[] passwordEnkripsi = bangkitPassword.enkripsi(password, salt);
            pemakai.setSalt(salt);
            pemakai.setPassword(passwordEnkripsi);
        }
        finally { 
        	pm.close(); 
        }                         
    }
    
    /**
     * Ubah peran seorang Pemakai
     * 
     * @param id    ID dari Pemakai yang hendak diubah peran-nya
     * @param peran Peran baru
     */
    public void ubahPeran(Long id, long peran) {
        // Buat key dari ID Pemakai 
        Key key = KeyFactory.createKey(Pemakai.class.getSimpleName(), id);
        
        // Ambil datanya
        PersistenceManager pm = PMF.getInstance().getPersistenceManager();
        try { 
            // Cari
            Pemakai pemakai = pm.getObjectById(Pemakai.class, key);
            // Ubah peran
            pemakai.setPeran(peran);
        }
        finally { 
        	pm.close(); 
        }                         
    }
  
    /**
     * Hapus seorang Pemakai
     * 
     * @param id ID dari pemakai yang akan dihapus
     * @exception javax.jdo.JDOObjectNotFoundException jika Pemakai dengan ID 
     *            yang diberikan tidak ditemukan
     */
    public void hapus(long id) {       
        // Buat key dari Pemakai 
        Key key = KeyFactory.createKey(Pemakai.class.getSimpleName(), id);
        
        // Ambil datanya
        PersistenceManager pm = PMF.getInstance().getPersistenceManager();
        Pemakai pemakai = null;
        try { 
            // Cari
            pemakai = pm.getObjectById(Pemakai.class, key);
            // Hapus
            pm.deletePersistent(pemakai);
        }
        finally { 
        	pm.close(); 
        }                         
    }
    
    /**
     * Ambil daftar Pemakai yang ada di Datastore. PERHATIKAN: PEmakai yang 
     * dikembalikan dalam list tidak bisa diubah karena PM-nya sudah ditutup!
     * 
     * @return List yang berisikan semua Pemakai
     */
    @SuppressWarnings("unchecked")
    public List<Pemakai> daftar() {
        // Cari PM-nya
        PersistenceManager pm = PMF.getInstance().getPersistenceManager();

        // Bangun query-nya
        Query query = pm.newQuery(Pemakai.class);
        
        // Jalankan query-nya
        List<Pemakai> hasil = null;
        try { 
            // Cari
            hasil = (List<Pemakai>) query.execute(); 
        }
        finally { pm.close(); }
        
        // Kembalikan daftar pemakai
        // PERHATIKAN: Object dalam hasil tidak bisa diubah karena PM-nya 
        //             sudah ditutup!
        return hasil; 
    }
    
    /**
     * Periksa apakah login yang diberikan benar atau tidak
     * 
     * @param username Username yang diberikan dari user yang akan login
     * @param password Password yang diberikan dari user yang akan login
     * 
     * @return Entiy {@link Pemakai} dari user dengan Username dan Password yang 
     * 		   diberikan.
     * 
     * @throws LoginSalah Informasi login salah
     * @throws NoSuchAlgorithmException Proses dekrepsi ada masalah
     * @throws InvalidKeySpecException Proses dekrepsi ada masalah
     */
    public Pemakai periksaLogin(String username, String password) 
    			   throws LoginSalah, 
    			   		  NoSuchAlgorithmException, 
    			   		  InvalidKeySpecException {
    	Pemakai pemakai = cariUsername(username);
    	if (pemakai == null)
    		throw new LoginSalah();
    	
    	SecureHashWithSalt secureHashWithSalt = new SecureHashWithSalt(); 
    	if (false == secureHashWithSalt.sama(password, 
    										 pemakai.getPassword(), 
    										 pemakai.getSalt()))
    		throw new LoginSalah();
    	
    	return pemakai;
    }
}