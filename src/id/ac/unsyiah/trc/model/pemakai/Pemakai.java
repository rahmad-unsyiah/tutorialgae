/**
 * Model untuk Pemakai
 * Pemakai adalah pihak-pihak yang bisa login ke dalam aplikasi.
 * 
 * Property:
 * + key      : Key dari si Pemakai
 * + username : Username dari si Pemakai
 * + password : Password untuk login
 * + salt     : Salt dari yang dipakai untuk melakukan secure-hash password 
 * 			    [READ ONLY]
 * + peran    : Jenis pemakai
 *              + Admin
 *              + PemakaiLain
 */
package id.ac.unsyiah.trc.model.pemakai;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Entity mewakili seorang Pemakai
 * 
 * Note: Dijadikan Serializable agar object dapat disimpan menjadi Session 
 *       Object dalam aplikasi.
 */
@PersistenceCapable
public class Pemakai implements Serializable {
	/** 
	 * PERAN_ADMIN jika user adalah admin dari aplikasi ini.
	 */
	public static final long PERAN_ADMIN = 1;
	/** 
	 * PERAN_BUKAN_ADMIN jika user bukan admin dari aplikasi ini.
	 */
	public static final long PERAN_BUKAN_ADMIN = 2;
	
	/**
	 * Constructor yang membuat Pemakai baru
	 * 
	 * @param username Username dari Pemakai baru
	 * @param password Password dari si user yang telah di secure hash
	 * @param salt     Salt yang dipergunakan untuk membangkitkan secure hash 
	 *                 dari password
	 * @param aktif    Status aktif dari Pemakai baru
	 */
	public Pemakai(String username, 
				   byte[] password, 
				   byte[] salt, 
				   long peran) {
		this.username = username;
		this.password = password;
		this.salt = salt;
		this.peran = peran;
	}
	
	/**
	 * Property: key [READ ONLY]
	 * Key dari Pemakai ini
	 */
    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Key key;
    public Key getKey() {
    	return key;
    }
    
    /**
     * Property: username [READ-ONLY]
     */
    @Persistent
    private String username;
    public String getUsername() {
    	return username;
    }
    
    /**
     * Property: password
     * Secure hash dari password si pemakai
     */
    @Persistent
    private byte[] password;
    public byte[] getPassword() {
    	return password;
    }
    public void setPassword(byte[] password) {
    	this.password = password;
    }

    /**
     * Property: salt
     * Salt yang dipakai untuk melakukan secure-hash password
     */
    @Persistent
    private byte[] salt;
    public byte[] getSalt() {
    	return salt;
    }
    public void setSalt(byte[] salt) {
    	this.salt = salt;
    }
    
    /**
     * Property: peran
     * Peran dari si Pemakai ini dalam sistem.
     */
    @Persistent
    private long peran;
    public long getPeran() {
    	return peran;
    }
    public void setPeran(long peran) {
    	this.peran = peran;
    }

	private static final long serialVersionUID = -4065602932993472828L;
}