package id.ac.unsyiah.trc.model;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

/**
 * Factory untuk membuat PersistenceManager yaitu object yang akan berkomunikasi
 * ke Datastore. Karena ini factory, kita harus data PersistenceManager yang 
 * dibuat untuk mengoptimalkan pemakaian resourse oleh karenanya PMF hanya boleh
 * ada satu dalam seluruh program atau dengan kata lain harus Singleton. 
 */
public class PMF {
	// Getter untuk mengambil PMF-nya.
	// Perhatikan tidak ada setter (setInstance) untuk menjaga agar object-nya
	// tidak diubah.
    public static PersistenceManagerFactory getInstance() {
        return instance;
    }
    
    // Nama dari persistence-manager-factory yang akan dipakai.
    // Lihat src/META-INF/jdoconfig.xml untuk konfigurasi 
    // persistence-manager-factory
    private static final String NAMA = "transactions-optional";
    
    // Satu-satunya object dari class ini (Singleton). 
    // Karena constructor-nya private hanya class ini yang bisa membuat dirinya 
    // sendiri dan cara membuatnya adalah seperti di bawah.
    private static final PersistenceManagerFactory instance =
           JDOHelper.getPersistenceManagerFactory(NAMA);
    
    // Constructor dijadikan private agar tidak ada class lain yang bisa buat.
    // Sehingga hanya class ini yang bisa membuat dirinya sendiri. 
    private PMF() { }
}