package id.ac.unsyiah.trc.model.gudang;

import java.util.List;
import java.util.ArrayList;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.datanucleus.annotations.Unowned;

import id.ac.unsyiah.trc.model.barang.Barang;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Entitas Gudang
 * 
 * Relasi
 * 
 *     +---------+
 *     | Pemakai |
 *     +---------+
 *          ^
 *         1|
 *          | unowned
 *         1|
 *      +--------+ 1        N +--------+
 *      | Gudang |----------->| Barang | 
 *      +--------+  unowned   +--------+
 *     
 *  + Gudang - Pemakai : 1-to-one unowned unidirectional
 *  + Gudang - Barang  : 1-to-many unowned unidirectional
 * 
 * Property:
 *   + key: PK
 *   + id: Versi dalam bentuk long. Tidak disimpan.  
 *   + nama: Nama gudang ini
 *   + lokasi: Lokasi gudang
 *   + manajer: {@link Pemakai} yang menjadi manajer gudang ini.
 *   + daftarBarang: {@link Barnag} yang disimpan dalam gudang ini.
 *   
 * Primary Key: id  
 */
@PersistenceCapable 
public class Gudang {
	public Gudang(String nama, String lokasi, Pemakai manajer) {
		this.nama = nama;
		this.lokasi = lokasi;
		this.manajer = manajer;
	}
	
	/**
	 * Primary key.
	 */
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	public Key getKey() {
		return key;
	}
	
	/**
	 * Property id
	 */
	public Long getId() {
		if (key == null)
			return null;
		return key.getId();
	}
	
	/**
	 * Property nama, yaitu nama dari gudang ini.
	 */
	@Persistent
	private String nama;
	public String getNama() {
    	return nama;
	}
	public void setNama(String nama) {
	    	this.nama = nama;
	}

	/**
	 * Property lokasi, yaitu alamat gudang ini.
	 */
	@Persistent
	private String lokasi;
	public String getLokasi() {
    	return lokasi;
	}
	public void setLokasi(String lokasi) {
	    	this.lokasi = lokasi;
	}
	
	/**
	 * Property manajer, yaitu manajer dari gudang ini. Bertipe {@link Pemakai}
	 */
	@Persistent
	@Unowned
	private Pemakai manajer = null;
	public Pemakai getManajer() {
		return manajer;
	}
	public void setManajer(Pemakai manajer) {
		this.manajer = manajer;
	}
	
	/**
	 * Property daftarBarang, yaitu daftar {@link Barang} yang dimiliki oleh 
	 * gudang ini. Bertipe {@link Barang}
	 */
	@Persistent
	@Unowned
	private List<Barang> daftarBarang;
	public List<Barang> getDaftarBarang() {
		if (daftarBarang == null) {
			daftarBarang = new ArrayList<Barang>();
		}
		
		return daftarBarang;
	}
	public void setDaftarBarang(List<Barang> daftarBarang) {
		if (daftarBarang == null) {
			this.daftarBarang = new ArrayList<Barang>();
		}
		else {
			this.daftarBarang = daftarBarang;
		}
			
	}
	
	
	/* 
	 * Default constructor tidak kita pakai
	 */
	@SuppressWarnings("unused")
	private Gudang() { }
}
