package id.ac.unsyiah.trc.model.gudang;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import id.ac.unsyiah.trc.model.PMF;
import id.ac.unsyiah.trc.model.barang.Barang;
import id.ac.unsyiah.trc.model.gudang.Gudang;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Controller untuk entity {@link Gudang}
 * 
 */
public class AturGudang {
	/**
	 * Tambah entity {@link Gudang} baru.
	 * 
	 * @param nama Nama dari {@link Gudang} yang akan ditambah. 
	 * @param lokasi Lokasi dari {@link Gudang} yang akan ditambah.
	 * @param manajer Menajer dari {@link Gudang} yang akan ditambah.
	 * 
	 * @return Mengembalikan entitas yang baru di buat.
	 */
	public Gudang baru(String nama, String lokasi, Pemakai manajer) {
		Gudang gudangBaru = new Gudang(nama, lokasi, manajer);

		// Minta factory untuk buat PersistenceManager untuk mengakses Datastore
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    try { 
	    	// Simpan di Datastore
	        pm.makePersistent(gudangBaru);
	    } 
	    finally { 
	    	// Tutup koneksi ke Datastore
	        pm.close();
	    }
	    
	    // Perhatikan perubahan pada object yang dikembalikan tidak akan 
	    // mengubah nilai di Datastore karena koneksi telah ditutup.
	    return gudangBaru;
	}
	
	/**
	 * Mencari satu entity {@link Gudang} di Datastore.
	 * 
	 * @param id ID dari entitas {@link Gudang} yang dicari.
	 * 
	 * @exception JDOObjectNotFoundException jika Entity tidak ditemukan
	 *  
	 * @return Entitas yang dicari atau null jika tidak ada.
	 */
	public Gudang cari(long id) throws JDOObjectNotFoundException {
		Gudang hasil = null;

		// Buatkan Key dari entitas dari id Barang yang diberikan
	    Key key = KeyFactory.createKey(Gudang.class.getSimpleName(), id);	

	    // Minta factory untuk buat PersistenceManager untuk mengakses Datastore	    
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    try { 
	    	// Ambil entitas-nya dari datastore
	        hasil = pm.getObjectById(Gudang.class, key);
	        
	        // Paksa ambil semua Object, dengan cara akses
	        for (Barang satuBarang : hasil.getDaftarBarang()) {
	        	satuBarang.getKey();
	        }
	    }
	    finally { 
	    	// Tutup koneksi ke Datastore
	    	pm.close(); 
	    }
	    
	    // Perhatikan perubahan pada object yang dikembalikan tidak akan 
	    // mengubah nilai di Datastore karena koneksi telah ditutup. 
	    return hasil;
	}
	
	/**
	 * Tambahkan {@link Barang} baru ke {@link Gudang}.
	 * 
	 * @param idGudang ID dari {@link Gudang} yang akan ditambahkan 
	 * 				   {@link Barang}
	 * @param idBarang ID dari {@link Barang} yang akan ditambahkan
	 * 
	 * @return {@link Gudang} yang baru ditambahkan {@link Barang} 
	 * 
	 * @throws JDOObjectNotFoundException jika {@link Barang}atau {@link Gudang}
	 * 									  tidak ditemukan
	 */
	public Gudang tambahBarang(long idGudang, long idBarang) 
				  throws JDOObjectNotFoundException {		
		Barang barang = null;		
		Gudang gudang = null;

		// Cari gudang berdasarkan id-nya
	    Key keyGudang = KeyFactory.createKey(Gudang.class.getSimpleName(), idGudang);	
	    Key keyBarang = KeyFactory.createKey(Barang.class.getSimpleName(), idBarang);	
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    try { 
	        gudang = pm.getObjectById(Gudang.class, keyGudang);
	        barang = pm.getObjectById(Barang.class, keyBarang);
	        
	        // Ketemu gudang dan barang, tambahkan ke daftarBarang
	        gudang.getDaftarBarang().add(barang);
	    }
	    finally { 
	    	// Tutup koneksi ke Datastore
	    	pm.close(); 
	    }
		
		return gudang;
	}
}