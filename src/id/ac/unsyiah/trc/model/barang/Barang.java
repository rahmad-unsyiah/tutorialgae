package id.ac.unsyiah.trc.model.barang;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

/**
 * Entitas Barang
 * 
 * Property:
 *   + id: Key 
 *   + kode: String
 *   + name: String
 *   + keterangan: String
 *   
 * Primary Key: id  
 */

@PersistenceCapable // Annotation @PersistenceCapable menandakan bahwa class ini
				    // adalah Entity yang harus disimpan dalam Datastore.
public class Barang {
	//
	// Constructor
	//
	
	@SuppressWarnings("unused") // Annotataion @SuppressWarnings("unused") untuk
								// hilangkan pesan peringatan bahwa constructor 
								// ini tidak pernah/bisa dipergunakan.
	private Barang() { } // Default constructor dijadikan private untuk 
						 // menghindari pembuatan object tanpa nilai awal.
	
	public Barang(String kode, String nama, String keterangan) {
		this.kode = kode;
		this.nama = nama;
		this.keterangan = keterangan;
	}
	
	
	//
	// Property kode
	//
	
	@Persistent // Anotation @Persisitent menandakan bahwa variable berikut akan
				// disimpan sebagai Property dari Class ini.
	private String kode; // Perhatikan variable-nya dijadikan private agar class
						 // bisa mengendalikan akses melalui fungs-fungsi getter
						 // dan setter.
	// Getter untuk property, untuk mengakses nilai dari variable.
	// Perhatikan nama variable memakai huruf k kecil sedangkan nama fungsi 
	// getter-nya memakai huruf K besar. 
	public String getKode() {
	    	return kode;
	}
	// Setter untuk property, untuk mengubah nilai dri variable.
	// Perhatikan nama variable memakai huruf k kecil sedangkan nama fungsi 
	// getter-nya memakai huruf K besar. 
	public void setKode(String kode) {
	    	this.kode = kode;
	}
	
	//
	// Property nama
	//
	
	@Persistent
	private String nama;
	public String getNama() {
    	return nama;
	}
	public void setNama(String nama) {
	    	this.nama = nama;
	}	

	
	//
	// Property keterangan
	//
	
	@Persistent
	private String keterangan;
	public String getKeterangan() {
    	return keterangan;
	}
	public void setKeterangan(String keterangan) {
	    	this.keterangan = keterangan;
	}
	
	
	//
	// Properti id
	//
	
	
	@PrimaryKey // Annotation @PrimaryKey menandakan bahwa property ini 
				// merupakan Primary Key entitas ini. 
	// Parameter valueStrategy dari Annotation @Persistent meminta agar nilai 
	// dari property dibangkitkan secara otomatis dan harus unik (IDENTITY).
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	// Hanya ada getter, tidak ada setter karena nilainya dibangkitkan secara 
	// otomatis 
	public Key getKey() {
		return key;
	}	
}