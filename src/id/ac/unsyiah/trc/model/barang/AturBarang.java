package id.ac.unsyiah.trc.model.barang;

import java.util.List;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import id.ac.unsyiah.trc.model.PMF;
import id.ac.unsyiah.trc.model.barang.Barang;

/**
 * Controller untuk entity {@link Barang}
 * 
 */
public class AturBarang {
	/**
	 * Tambah entity {@link Barang} baru.
	 * 
	 * @param kode Kode dari {@link Barang} yang akan ditambah. 
	 * @param nama Nama dari {@link Barang} yang akan ditambah.
	 * @param keterangan Keterangan dari {@link Barang} yang akan ditambah.
	 * 
	 * @return Mengembalikan entitas yang baru di buat.
	 */
	public Barang baru(String kode, String nama, String keterangan) {
		// Buat entitas baru
	    Barang barangBaru = new Barang(kode, nama, keterangan);
	    
	    // Minta factory untuk buat PersistenceManager untuk mengakses Datastore
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    try { 
	    	// Simpan di Datastore
	        pm.makePersistent(barangBaru);
	    } 
	    finally { 
	    	// Tutup koneksi ke Datastore
	        pm.close();
	    }
	    // Perhatikan perubahan pada object yang dikembalikan tidak akan 
	    // mengubah nilai di Datastore karena koneksi telah ditutup. 
	    return barangBaru;
	}
	
	/**
	 * Ubah sebuah entity {@link Barang}.
	 * 
	 * @param id ID dari entitas {@link Barang} yang akan diubah.
	 * @param kodeBaru Kode baru dari {@link Barang}
	 * @param namaBaru Nama baru dari {@link Barang}
	 * @param keteranganBaru Keterangan baru dari {@link Barang}
	 * 
	 * @exception JDOObjectNotFoundException jika Entity tidak ditemukan
	 */
	public void ubah(long id, 
					 String kodeBaru, 
					 String namaBaru, 
					 String keteranganBaru) 
				throws JDOObjectNotFoundException {
		// Buatkan Key dari entitas dari id Barang yang diberikan
	    Key key = KeyFactory.createKey(Barang.class.getSimpleName(), id);
	    
	    // Minta factory untuk buat PersistenceManager untuk mengakses Datastore	    
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    try {
	    	// Ambil entitas-nya dari datastore
	    	Barang barang = pm.getObjectById(Barang.class, key);
	    	// Lakukan perubahan
	    	barang.setKode(kodeBaru);
	    	barang.setNama(namaBaru);
	    	barang.setKeterangan(keteranganBaru);
	    }
	    finally { 
	    	// Tutup koneksi ke Datastore
	    	pm.close(); 
	    }		
	}
	
	/**
	 * Menghapus sebuah entitas {@link Barang}.
	 *  
	 * @param id ID dari entitas {@link Barang} yang akan dihapus.
	 * 
	 * @exception JDOObjectNotFoundException jika Entity tidak ditemukan
	 */
	public void hapus(long id) throws JDOObjectNotFoundException {       
		// Buatkan Key dari entitas dari id Barang yang diberikan
	    Key key =  KeyFactory.createKey(Barang.class.getSimpleName(), id);
	    
	    // Minta factory untuk buat PersistenceManager untuk mengakses Datastore	    
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    try { 
	    	// Ambil entitas-nya dari datastore
	        Barang barang = pm.getObjectById(Barang.class, key);
	        // Minta entitas dihapus dari Datastore
	        pm.deletePersistent(barang);
	    }
	    finally { 
	    	// Tutup koneksi ke Datastore
	    	pm.close(); 
	    }                         
	}
	
	/**
	 * Mencari satu entity {@link Barang} di Datastore.
	 * 
	 * @param id ID dari entitas {@link Barang} yang dicari.
	 * 
	 * @exception JDOObjectNotFoundException jika Entity tidak ditemukan
	 *  
	 * @return Entitas yang dicari atau null jika tidak ada.
	 */
	public Barang cari(long id) throws JDOObjectNotFoundException {
	    Barang barang = null;

	    // Buatkan Key dari entitas dari id Barang yang diberikan
	    Key key = KeyFactory.createKey(Barang.class.getSimpleName(), id);	

	    // Minta factory untuk buat PersistenceManager untuk mengakses Datastore	    
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    try { 
	    	// Ambil entitas-nya dari datastore
	        barang = pm.getObjectById(Barang.class, key);
	    }
	    finally { 
	    	// Tutup koneksi ke Datastore
	    	pm.close(); 
	    }
	    
	    // Perhatikan perubahan pada object yang dikembalikan tidak akan 
	    // mengubah nilai di Datastore karena koneksi telah ditutup. 
	    return barang;
	}
	
	/**
	 * Mencari satu entity {@link Barang} di Datastore berdasarkan kode-nya.
	 * 
	 * @param kode Kode dari entitas {@link Barang} yang dicari.
	 * 
	 * @exception JDOObjectNotFoundException jika Entity tidak ditemukan
	 *  
	 * @return Entitas yang dicari atau null jika tidak ada.
	 */
	@SuppressWarnings("unchecked")
	public Barang cari(String kode) throws JDOObjectNotFoundException {
	    List<Barang> hasil = null;

	    // Minta factory untuk buat PersistenceManager untuk mengakses Datastore	    
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    // Buat query baru untuk Barang: ambil semua entitas dengan kriteria 
	    // kode == kodeCari
	    Query query = pm.newQuery(Barang.class);
	    query.declareParameters("String kodeCari");
	    query.setFilter("kode == kodeCari");
	    try { 
	    	// Jalankan query dan berikan nilai untuk kodeCari. Perhatikan 
	    	// pengembalian dalam bentuk list.
	    	hasil = (List<Barang>) query.execute(kode); 
	    }
	    finally { 
	    	// Tutup koneksi ke Datastore
	    	pm.close(); 
	    }
	    
	    // Jika kosong kembalikan null
	    if (hasil.isEmpty() == true) 
	    	return null;
	    // Jika ada lebih dari satu hasil juga kembalikan null. Seharusnya kode 
	    // barang harus unique.
	    if (hasil.size() > 1)
	    	return null;
	    // Kembalikan elemen pertama, karena hasil itu bertipe List.
	    return hasil.get(0);
	}
	
	/**
	 * Kembalikan semua entitas {@link Barang} yang ada di Datastore.
	 *  
	 * @return List dari semua entitas {@link Barang} yang ada di Datastore.
	 */
	@SuppressWarnings("unchecked") // Annotation ini untuk menghilangkan warning
								   // yang muncul di cast saat query 
								   // mengembalikan hasilnya. 
	public List<Barang> daftar() {
	    List<Barang> hasil = null;

	    // Minta factory untuk buat PersistenceManager untuk mengakses Datastore	    
	    PersistenceManager pm = PMF.getInstance().getPersistenceManager();
	    // Buat query baru untuk Barang: ambil semua entitas  
	    Query query = pm.newQuery(Barang.class);
	    try { 
	    	// Jalankan query. Perhatikan pengembalian dalam bentuk list.
	        hasil = (List<Barang>) query.execute();
	    }
	    finally { 
	    	// Tutup koneksi ke Datastore
	        pm.close(); 
	    }

	    // Perhatikan perubahan pada list object yang dikembalikan tidak akan 
	    // mengubah nilai di Datastore karena koneksi telah ditutup. 
	    return hasil; 
	}
}