package id.ac.unsyiah.trc.webuji.barang;

import static net.sourceforge.jwebunit.junit.JWebUnit.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UjiDaftar {
    @Before // Annotation ini untuk menandakan bahwa sebelum menjalankan satu 
    		// @Test jalankan method berikut ini terlebih dahulu
	public void setUp(){
    	//Set terlebih dahulu URL dasar yang digunakan
		setBaseUrl("http://localhost:8888"); //Set terlebih dahulu url yang digunakan
	}

    @After // Annotation ini untuk menandakan bahwa sesudah menjalankan satu 
    	   // @Test jalankan method berikut ini
	@Before
    public void tearDown() {
		
	}
    
	@Test
    public void daftarKosong() {
		// Buka halaman index.html
		beginAt("index.html");
		
		// Cari link untuk ke halaman daftar barang
		assertLinkPresent("pgDaftarBarang");
		// Clink link tersebut
		clickLink("pgDaftarBarang");
		// Periksa masuk ke halaman Daftar Barang
		assertTitleEquals("Daftar Barang");
		// Periksa ada tabel
		assertTablePresent("tblDaftarBarang");
		// Pastikan row kosong
		assertTableRowCountEquals("tblDaftarBarang", 1);
    }

	@Test
    public void daftarBarang1() {
		// Buka halaman index.html
		beginAt("index.html");
		
		// Clink link ke halaman daftar barang
		clickLink("pgDaftarBarang");
		// Cari tombol "Tambah barang"
		assertButtonPresent("btnTambahBarang");
		// Click tombol "Tambah Barang"
		clickButton("btnTambahBarang");
		// Pastikan ada di halaman "Tambah Barang"
		assertTitleEquals("Tambah Barang");
		
		// Pastikan ada form tambah barang
		assertFormPresent("frmTambahBarang");
		// Pastikan ada elemen untuk kode
		assertFormElementPresent("txtKode");
		assertFormElementMatch("txtKode", "");
		// Pastikan ada elemen untuk nama
		assertFormElementPresent("txtNama");
		assertFormElementMatch("txtNama", "");
		// Pastikan ada elemen untuk keterangan
		assertFormElementPresent("txtKeterangan");
		assertTextInElement("txtKeterangan", "");
		// Pastikan ada tombol Tambah
		assertFormElementPresent("btnSubmit");
		// Pastikan ada tombol batal
		assertButtonPresent("btnCancel");
		
		// Isi form
		setTextField("txtKode", kode);
		setTextField("txtNama", nama);
		setTextField("txtKeterangan", keterangan);
		// Kirim form tambah barang
		submit();
		
		// Pastikan di halaman daftar barang
		assertTitleEquals("Daftar Barang");
		// Tunggu data tersimpan di datastore
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		
		// Pastikan ada 1 row
		assertTableRowCountEquals("tblDaftarBarang", 2);
		// Pastikan isi data benar
		String[][] baris1 = {{kode, nama, keterangan, "Ubah Hapus"}};
		assertTableRowsEqual("tblDaftarBarang", 1, baris1);
		
		// Pastikan ada tombol hapus
		assertButtonPresent("btnHapus");
		// Click tombol hapus
		clickButton("btnHapus");
		// Pastikan sudah di halaman hapus barang
		assertTitleEquals("Hapus Barang");
				
		// Pastikan ada form hapus barang
		assertFormPresent("frmHapusBarang");
		// Pastikan ada elemen untuk hdnId
		assertFormElementPresent("hdnId");
		// Pastikan ada elemen untuk kode
		assertFormElementPresent("txtKode");
		assertFormElementMatch("txtKode", kode);
		// Pastikan ada elemen untuk nama
		assertFormElementPresent("txtNama");
		assertFormElementMatch("txtNama", nama);
		// Pastikan ada elemen untuk keterangan
		assertFormElementPresent("txtKeterangan");
		assertTextInElement("txtKeterangan", keterangan);
		// Pastikan ada tombol Hapus
		assertFormElementPresent("btnSubmit");
		// Pastikan ada tombol batal
		assertButtonPresent("btnCancel");
		// Kirim form hapus barang
		submit();
		
		// Pastikan di halaman daftar barang
		assertTitleEquals("Daftar Barang");
		// Tunggu data tersimpan di datastore
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");

		// Pastikan row kosong
		assertTableRowCountEquals("tblDaftarBarang", 1);
	}
	
	@Test
    public void daftarBarang1Ubah() {
		// Buka halaman index.html
		beginAt("index.html");
		
		// Ke halaman daftar barang
		clickLink("pgDaftarBarang");		
		
		// Tambah barang
		clickButton("btnTambahBarang");
		setTextField("txtKode", kode);
		setTextField("txtNama", nama);
		setTextField("txtKeterangan", keterangan);
		submit();
		
		// Tunggu data tersimpan di datastore
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		
		// Pastikan ada tombol ubah
		assertButtonPresent("btnUbah");
		// Click tombol hapus
		clickButton("btnUbah");
		// Pastikan sudah di halaman ubah barang
		assertTitleEquals("Ubah Barang");
		
		// Pastikan ada form Ubah barang
		assertFormPresent("frmUbahBarang");
		// Pastikan ada elemen untuk hdnId
		assertFormElementPresent("hdnId");
		// Pastikan ada elemen untuk kode
		assertFormElementPresent("txtKode");
		assertFormElementMatch("txtKode", kode);
		// Pastikan ada elemen untuk nama
		assertFormElementPresent("txtNama");
		assertFormElementMatch("txtNama", nama);
		// Pastikan ada elemen untuk keterangan
		assertFormElementPresent("txtKeterangan");
		assertTextInElement("txtKeterangan", keterangan);
		// Pastikan ada tombol Hapus
		assertFormElementPresent("btnSubmit");
		// Pastikan ada tombol batal
		assertButtonPresent("btnCancel");

		// Ubah form
		setTextField("txtKode", kode2);
		setTextField("txtNama", nama2);
		setTextField("txtKeterangan", keterangan2);
		// Kirim form ubah barang
		submit();
		
		// Pastikan di halaman daftar barang
		assertTitleEquals("Daftar Barang");
		// Tunggu data tersimpan di datastore
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		
		// Pastikan ada 1 row
		assertTableRowCountEquals("tblDaftarBarang", 2);
		// Pastikan isi data benar
		String[][] baris1 = {{kode2, nama2, keterangan2, "Ubah Hapus"}};
		assertTableRowsEqual("tblDaftarBarang", 1, baris1);
		
		// Hapus barang
		clickButton("btnHapus");
		submit();
		
		// Tunggu data tersimpan di datastore
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");
		gotoPage("/barang");

		// Pastikan row kosong
		assertTitleEquals("Daftar Barang");
		assertTableRowCountEquals("tblDaftarBarang", 1);		
	}
	
	private String kode = "Kode1";
	private String nama = "Nama1";
	private String keterangan = "Keterangan1";

	private String kode2 = "Kode2";
	private String nama2 = "Nama2";
	private String keterangan2 = "Keterangan2";
}
