/**
 * Exception untuk tercatatnya username ganda di datastore
 */
package id.ac.unsyiah.trc.eksepsi;

import javax.jdo.JDODataStoreException;

public class LoginSalah extends JDODataStoreException {
	public LoginSalah() {
        super();
    }

    public LoginSalah(String msg) {
        super(msg);
    }

    public LoginSalah(String msg, Throwable[] nested) {
        super(msg, nested);
    }

    public LoginSalah(String msg, Throwable nested) {
        super(msg, nested);
    }

    public LoginSalah(String msg, Object failed) {
        super(msg, failed);
    }

    public LoginSalah(String msg, Throwable[] nested, Object failed) {
        super(msg, nested, failed);
    }

    public LoginSalah(String msg, Throwable nested, Object failed) {
        super(msg, nested, failed);
    }

	private static final long serialVersionUID = -2531107820741215818L;
}