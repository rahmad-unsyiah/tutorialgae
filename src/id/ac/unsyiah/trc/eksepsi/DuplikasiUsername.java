/**
 * Exception untuk tercatatnya username ganda di datastore
 */
package id.ac.unsyiah.trc.eksepsi;

import javax.jdo.JDODataStoreException;

public class DuplikasiUsername extends JDODataStoreException {
	public DuplikasiUsername() {
        super();
    }

    public DuplikasiUsername(String msg) {
        super(msg);
    }

    public DuplikasiUsername(String msg, Throwable[] nested) {
        super(msg, nested);
    }

    public DuplikasiUsername(String msg, Throwable nested) {
        super(msg, nested);
    }

    public DuplikasiUsername(String msg, Object failed) {
        super(msg, failed);
    }

    public DuplikasiUsername(String msg, Throwable[] nested, Object failed) {
        super(msg, nested, failed);
    }

    public DuplikasiUsername(String msg, Throwable nested, Object failed) {
        super(msg, nested, failed);
    }

	private static final long serialVersionUID = -3968147086612379023L;
}