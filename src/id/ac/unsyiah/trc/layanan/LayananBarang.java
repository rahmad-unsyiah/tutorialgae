package id.ac.unsyiah.trc.layanan;

import java.util.List;

import javax.jdo.JDOObjectNotFoundException;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.response.NotFoundException;

import id.ac.unsyiah.trc.model.barang.AturBarang;
import id.ac.unsyiah.trc.model.barang.Barang;

/**
 * Class untuk menangani Layanan Web. Akan memanggil method yang sesuai dari 
 * {@link AturBarang}. 
 *
 */
@Api(name="layananbarang",						// Nama layanan ini
	 title="Layanan Barang", 					// Judul yang muncul di Explorer
	 version="v1", 								// Versi
	 description="Layanan Web untuk Barang")	// Penjelasan ringkas
public class LayananBarang {
	/**
	 * Layanan Web untuk membuat barang baru
	 * 
	 * @param kode Kode dari {@link Barang} yang akan ditambah. 
	 * @param nama Nama dari {@link Barang} yang akan ditambah.
	 * @param keterangan Keterangan dari {@link Barang} yang akan ditambah.
	 * 
	 * @return Mengembalikan entitas yang baru di buat.
	 */
	@ApiMethod(name="baru",						// Nama Method untuk layanan ini 
			   httpMethod=HttpMethod.POST)		// Dipanggil pakai perintah apa
	public Barang baru(@Named("kode") String kode, 
					   @Named("nama") String nama, 
					   @Named("keterangan") String keterangan) {
		return new AturBarang().baru(kode, nama, keterangan);
	}

	/**
	 * Layanan wen untuk mengubah sebuah entity {@link Barang}.
	 * 
	 * @param id ID dari entitas {@link Barang} yang akan diubah.
	 * @param kodeBaru Kode baru dari {@link Barang}
	 * @param namaBaru Nama baru dari {@link Barang}
	 * @param keteranganBaru Keterangan baru dari {@link Barang}
	 * 
	 * @exception NotFoundException jika {@link Barang} dengan ID tidak 
	 *                              ditemukan
	 */
	@ApiMethod(name="ubah", httpMethod=HttpMethod.PUT)	
	public void ubah(@Named("id") long id, 
					 @Named("kodeBaru") String kodeBaru,
					 @Named("namaBaru") String namaBaru,
					 @Named("keteranganBaru") String keteranganBaru) 
				throws NotFoundException {		
		try {
			new AturBarang().ubah(id, kodeBaru, namaBaru, keteranganBaru);
		}
		catch(JDOObjectNotFoundException kesalahan) {
			throw new NotFoundException("Barang tidak ditemukan!!!");
		}
	}

	/**
	 * Layanan web untuk menghapus sebuah entitas {@link Barang}.
	 *  
	 * @param id ID dari entitas {@link Barang} yang akan dihapus.
	 * 
	 * @exception NotFoundException jika {@link Barang} dengan ID tidak 
	 *                              ditemukan
	 */
	@ApiMethod(name="hapus", httpMethod=HttpMethod.DELETE)	
	public void hapus(@Named("id") long id) throws NotFoundException {		
		try {
			new AturBarang().hapus(id);
		}
		catch(JDOObjectNotFoundException kesalahan) {
			throw new NotFoundException("Barang tidak ditemukan!!!");
		}
	}

	/**
	 * Layanan web untuk mencari satu entity {@link Barang} di Datastore.
	 * 
	 * @param id ID dari entitas {@link Barang} yang dicari.
	 * 
	 * @exception NotFoundException jika {@link Barang} dengan ID tidak 
	 *                              ditemukan
	 *  
	 * @return {@link Barang} yang dicari
	 */
	@ApiMethod(name="cari", httpMethod=HttpMethod.GET)
	public Barang cari(@Named("id") long id) throws NotFoundException {
		try {
			return new AturBarang().cari(id);
		}
		catch(JDOObjectNotFoundException kesalahan) {
			throw new NotFoundException("Barang tidak ditemukan!!!");
		}
	}

	/**
	 * Layanan web untuk mencari satu entity {@link Barang} di Datastore 
	 * berdasarkan kode-nya.
	 * 
	 * @param kode Kode dari entitas {@link Barang} yang dicari.
	 * 
	 * @exception NotFoundException jika {@link Barang} tidak ditemukan
	 *  
	 * @return {@link Barang} yang dicari atau null jika tidak ada.
	 */
	@ApiMethod(name="cariKode", httpMethod=HttpMethod.GET)
	public Barang cariKode(@Named("kode") String kode) throws NotFoundException {
		try {
			return new AturBarang().cari(kode);
		}
		catch(JDOObjectNotFoundException kesalahan) {
			throw new NotFoundException("Barang tidak ditemukan!!!");
		}
	}

	/**
	 * Layanan web untuk kembalikan semua entitas {@link Barang} yang ada di 
	 * Datastore.
	 *  
	 * @return List dari semua entitas {@link Barang} yang ada di Datastore.
	 */
	@ApiMethod(name="daftar", httpMethod=HttpMethod.GET)
	public List<Barang> daftar(){
		return new AturBarang().daftar();
	}
}