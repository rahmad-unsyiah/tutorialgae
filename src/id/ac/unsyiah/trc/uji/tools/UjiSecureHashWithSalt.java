/**
 * Unit test untuk kelas ctr.Password
 */
package id.ac.unsyiah.trc.uji.tools;

import static org.junit.Assert.assertEquals;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import id.ac.unsyiah.trc.tools.SecureHashWithSalt;

public class UjiSecureHashWithSalt {
	@Before
    public void setUp() { }
    @After
    public void tearDown() { }
    
    /*
     * Otentikasi yang berhasil 
     */
    @Test
	public void testOtentikasi() 
				throws NoSuchAlgorithmException, InvalidKeySpecException {
    	// Secure-hash password
    	SecureHashWithSalt pass = new SecureHashWithSalt();
    	String password = "Pass1";
    	byte[] salt = pass.buatSalt();
    	byte[] enkripsi = pass.enkripsi(password, salt);
    	
    	// Uji
		assertEquals(pass.sama(password, enkripsi, salt), true);
    }

    /*
     * Otentikasi yang gagal karena salt-nya beda 
     */
    @Test
	public void testOtentikasiBedaSalt() 
				throws NoSuchAlgorithmException, InvalidKeySpecException {
    	// Secure-hash password
    	SecureHashWithSalt pass = new SecureHashWithSalt();
    	String password = "Pass1";
    	byte[] salt = pass.buatSalt();
    	byte[] saltLain = pass.buatSalt();
    	byte[] enkripsi = pass.enkripsi(password, salt);
    	
    	// Uji
		assertEquals(pass.sama(password, enkripsi, saltLain), false);
    }

    /*
     * Otentikasi yang gagal karena password-nya beda 
     */
    @Test
	public void testOtentikasiBedaPassword() 
				throws NoSuchAlgorithmException, InvalidKeySpecException {
    	// Secure-hash password
    	SecureHashWithSalt pass = new SecureHashWithSalt();
    	String password = "Pass1";
    	String passwordLain = "Pass2";
    	byte[] salt = pass.buatSalt();
    	byte[] enkripsi = pass.enkripsi(password, salt);
    	
    	// Uji
		assertEquals(pass.sama(passwordLain, enkripsi, salt), false);
    }
}