package id.ac.unsyiah.trc.uji.model.gudang;

import org.junit.After;
import org.junit.Before; 
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.jdo.JDOObjectNotFoundException;

import id.ac.unsyiah.trc.eksepsi.DuplikasiUsername;
import id.ac.unsyiah.trc.model.barang.AturBarang;
import id.ac.unsyiah.trc.model.barang.Barang;
import id.ac.unsyiah.trc.model.gudang.AturGudang;
import id.ac.unsyiah.trc.model.gudang.Gudang;
import id.ac.unsyiah.trc.model.pemakai.AturPemakai;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Unittest untuk menguji Entity Gudang
 */
public class UjiAturGudang {
    @Before
    public void setUp() throws DuplikasiUsername, 
    						   NoSuchAlgorithmException, 
    						   InvalidKeySpecException { 
        // Untuk GAE
        testHelper.setUp();

        // Buat controller pemakai
        aturPemakai = new AturPemakai();
        // Buat pemakai
    	manajer = aturPemakai.baru(username, password, Pemakai.PERAN_BUKAN_ADMIN);

        // Buat controller barang
        aturBarang = new AturBarang();
        // Buat Barang
    	barang1 = aturBarang.baru(kodeBarang1, namaBarang1, keteranganBarang1);
    	barang2 = aturBarang.baru(kodeBarang2, namaBarang2, keteranganBarang2);
    	barang3 = aturBarang.baru(kodeBarang3, namaBarang3, keteranganBarang3);
    	
    	// Buat controller Gudang
    	aturGudang = new AturGudang();
    }
    
    @After
    public void tearDown() {

        // Untuk GAE
        testHelper.tearDown(); 
    }
    
    @Test
    public void baru() {
    	gudang = aturGudang.baru(namaGudang1, lokasiGudang1, manajer);
    	
    	// Uji
    	assertNotNull(gudang);
    	assertNotNull(gudang.getKey());
    	assertNotNull(gudang.getManajer());
    	assertNotNull(gudang.getManajer().getKey());
    	assertEquals(gudang.getManajer().getKey().getId(), manajer.getKey().getId());
    	assertEquals(gudang.getNama(), namaGudang1);
    	assertEquals(gudang.getLokasi(), lokasiGudang1);
    }
    
    @Test(expected=JDOObjectNotFoundException.class)
    public void cariTidakKetemuDataStoreKosong() {
    	aturGudang.cari(-1l);
    }
    
    @Test(expected=JDOObjectNotFoundException.class)
    public void cariTidakKetemuDataStoreTidakKosong() {
    	aturGudang.baru(namaGudang1, lokasiGudang1, manajer);
    	aturGudang.baru(namaGudang2, lokasiGudang2, manajer);
    	aturGudang.baru(namaGudang3, lokasiGudang3, manajer);
    	aturGudang.cari(-1l);
    }
    
    @Test
    public void cariKetemu() {
    	gudang = aturGudang.baru(namaGudang1, lokasiGudang1, manajer);
    	aturGudang.baru(namaGudang2, lokasiGudang2, manajer);
    	aturGudang.baru(namaGudang3, lokasiGudang3, manajer);
    	
    	Gudang hasilCari = aturGudang.cari(gudang.getKey().getId());
    	// Uji
    	assertNotNull(hasilCari);
    	assertNotNull(hasilCari.getKey());
    	assertEquals(hasilCari.getKey().getId(), gudang.getKey().getId());
    }
    
    @Test(expected=JDOObjectNotFoundException.class)
    public void tambahBarangTidakKetemuBarang() {
    	gudang = aturGudang.baru(namaGudang1, lokasiGudang1, manajer);
    	aturGudang.tambahBarang(gudang.getKey().getId(), -1);
    }
    
    @Test(expected=JDOObjectNotFoundException.class)
    public void tambahBarangTidakKetemuGudang() {
    	aturGudang.tambahBarang(-1, barang1.getKey().getId());
    }
    
    @Test
    public void tambahBarang() {
    	gudang = aturGudang.baru(namaGudang1, lokasiGudang1, manajer);
    	// Pastika jumlah barang 0
    	assertNotNull(gudang);
    	assertNotNull(gudang.getDaftarBarang());
    	assertEquals(gudang.getDaftarBarang().size(), 0);
    	
    	// Tambah 1 barang
    	aturGudang.tambahBarang(gudang.getKey().getId(), barang1.getKey().getId());
    	// Uji
    	Gudang gudangCari = aturGudang.cari(gudang.getKey().getId()); // Harus cari lagi karena sudah berubah di datastore 
    	assertNotNull(gudangCari.getDaftarBarang());
    	assertEquals(gudangCari.getDaftarBarang().size(), 1);    	
    	assertNotNull(gudangCari.getDaftarBarang().get(0));
    	assertNotNull(gudangCari.getDaftarBarang().get(0).getKey());
    	assertEquals(gudangCari.getDaftarBarang().get(0).getKey().getId(), barang1.getKey().getId());

    	// Tambah 1 barang jadi 2
    	aturGudang.tambahBarang(gudang.getKey().getId(), barang2.getKey().getId());
    	// Uji
    	gudangCari = aturGudang.cari(gudang.getKey().getId()); // Harus cari lagi karena sudah berubah di datastore 
    	assertNotNull(gudangCari.getDaftarBarang());
    	assertEquals(gudangCari.getDaftarBarang().size(), 2);    	
    	assertNotNull(gudangCari.getDaftarBarang().get(0));
    	assertNotNull(gudangCari.getDaftarBarang().get(0).getKey());
    	assertEquals(gudangCari.getDaftarBarang().get(0).getKey().getId(), barang1.getKey().getId());
    	assertNotNull(gudangCari.getDaftarBarang().get(1));
    	assertNotNull(gudangCari.getDaftarBarang().get(1).getKey());
    	assertEquals(gudangCari.getDaftarBarang().get(1).getKey().getId(), barang2.getKey().getId());

    	// Tambah 1 barang jadi 3
    	aturGudang.tambahBarang(gudang.getKey().getId(), barang3.getKey().getId());
    	// Uji
    	gudangCari = aturGudang.cari(gudang.getKey().getId()); // Harus cari lagi karena sudah berubah di datastore 
    	assertNotNull(gudangCari.getDaftarBarang());
    	assertEquals(gudangCari.getDaftarBarang().size(), 3);
    	assertNotNull(gudangCari.getDaftarBarang().get(0));
    	assertNotNull(gudangCari.getDaftarBarang().get(0).getKey());
    	assertEquals(gudangCari.getDaftarBarang().get(0).getKey().getId(), barang1.getKey().getId());
    	assertNotNull(gudangCari.getDaftarBarang().get(1));
    	assertNotNull(gudangCari.getDaftarBarang().get(1).getKey());
    	assertEquals(gudangCari.getDaftarBarang().get(1).getKey().getId(), barang2.getKey().getId());
    	assertNotNull(gudangCari.getDaftarBarang().get(2));
    	assertNotNull(gudangCari.getDaftarBarang().get(2).getKey());
    	assertEquals(gudangCari.getDaftarBarang().get(2).getKey().getId(), barang3.getKey().getId());
    }

    private final LocalServiceTestHelper testHelper = 
    		new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    // Data uji pemakai
    private AturPemakai aturPemakai;
    private Pemakai manajer = null;
    
    private static final String username = "SI-A";
    private static final String password = "SI-B";
    
    // Data Gudang
    private AturGudang aturGudang;
    private Gudang gudang = null;
    
    private static final String namaGudang1 = "NAMA GUDANG 1";
    private static final String lokasiGudang1 = "LOKASI-XYZ 1";
    private static final String namaGudang2 = "NAMA GUDANG 2";
    private static final String lokasiGudang2 = "LOKASI-XYZ 2";
    private static final String namaGudang3 = "NAMA GUDANG 3";
    private static final String lokasiGudang3 = "LOKASI-XYZ 3";
    
    // Data barang
    private AturBarang aturBarang;
    private Barang barang1 = null;
    private Barang barang2 = null;
    private Barang barang3 = null;
    
    private static final String kodeBarang1 = "KODE-BARANG-1";
    private static final String kodeBarang2 = "KODE-BARANG-2";
    private static final String kodeBarang3 = "KODE-BARANG-3";
    
    private static final String namaBarang1 = "NAMA-BARANG-1";
    private static final String namaBarang2 = "NAMA-BARANG-2";
    private static final String namaBarang3 = "NAMA-BARANG-3";
    
    private static final String keteranganBarang1 = "KETERANGAN-BARANG-1";
    private static final String keteranganBarang2 = "KETERANGAN-BARANG-2";
    private static final String keteranganBarang3 = "KETERANGAN-BARANG-3";
}
