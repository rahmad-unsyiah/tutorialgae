package id.ac.unsyiah.trc.uji.model.gudang;

import org.junit.After;
import org.junit.Before; 
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import id.ac.unsyiah.trc.eksepsi.DuplikasiUsername;
import id.ac.unsyiah.trc.model.gudang.Gudang;
import id.ac.unsyiah.trc.model.pemakai.AturPemakai;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Unittest untuk menguji Entity Gudang
 */
public class UjiGudang {
    @Before
    public void setUp() throws DuplikasiUsername, 
    						   NoSuchAlgorithmException, 
    						   InvalidKeySpecException { 
        // Untuk GAE
        testHelper.setUp();

        // Buat controller pemakai
        aturPemakai = new AturPemakai();
        // Buat dua pemakai
    	manajer1 = aturPemakai.baru(username1, password1, Pemakai.PERAN_BUKAN_ADMIN);
    	manajer2 = aturPemakai.baru(username2, password2, Pemakai.PERAN_BUKAN_ADMIN);
    }
    
    @After
    public void tearDown() {
    	// Hapus data
    	aturPemakai.hapus(manajer1.getKey().getId());
    	aturPemakai.hapus(manajer2.getKey().getId());

        // Untuk GAE
        testHelper.tearDown(); 
    }
    
    /**
     * Uji constructor.
     */
    @Test
    public void constructor() {
    	// Buat object-nya
    	Gudang gudang = new Gudang(namaUji, lokasiUji, manajer1);
    	
    	// Uji benar apa tidak isinya
    	assertEquals(gudang.getNama(), namaUji);
    	assertEquals(gudang.getLokasi(), lokasiUji);
    	assertNotNull(gudang.getManajer());
    	assertNotNull(gudang.getManajer().getKey());
    	assertEquals(gudang.getManajer().getKey().getId(), manajer1.getKey().getId());
    	assertNotNull(gudang.getDaftarBarang());
    	assertEquals(gudang.getDaftarBarang().size(), 0);
    }
    
    /**
     * Uji property nama
     */
    @Test
    public void nama() {
    	// Buat object-nya
    	Gudang gudang = new Gudang(namaUji, lokasiUji, manajer1);
    	
    	// Uji benar apa tidak isinya
    	assertEquals(gudang.getNama(), namaUji);
    	
    	// Coba ubah property-nya
    	gudang.setNama(namaUji2);
    	// Uji benar apa tidak isinya
    	assertEquals(gudang.getNama(), namaUji2);
    }
    
    /**
     * Uji property lokasi
     */
    @Test
    public void lokasi() {
    	// Buat object-nya
    	Gudang gudang = new Gudang(namaUji, lokasiUji, manajer1);
    	
    	// Uji benar apa tidak isinya
    	assertEquals(gudang.getLokasi(), lokasiUji);
    	
    	// Coba ubah property-nya
    	gudang.setLokasi(lokasiUji2);
    	// Uji benar apa tidak isinya
    	assertEquals(gudang.getLokasi(), lokasiUji2);
    }
    
    /**
     * Uji property manajer
     */
    @Test
    public void manajer() {
    	// Buat object-nya
    	Gudang gudang = new Gudang(namaUji, lokasiUji, manajer1);
    	
    	// Uji benar apa tidak isinya
    	assertNotNull(gudang.getManajer());
    	assertNotNull(gudang.getManajer().getKey());
    	assertEquals(gudang.getManajer().getKey().getId(), manajer1.getKey().getId());
    	
    	// Coba ubah property-nya
    	gudang.setManajer(manajer2);
    	// Uji benar apa tidak isinya
    	assertNotNull(gudang.getManajer());
    	assertNotNull(gudang.getManajer().getKey());
    	assertEquals(gudang.getManajer().getKey().getId(), manajer2.getKey().getId());
    }

    private final LocalServiceTestHelper testHelper = 
    		new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    // Data uji pemakai
    private AturPemakai aturPemakai;
    private Pemakai manajer1 = null;
    private Pemakai manajer2 = null;
    
    private static final String username1 = "SI-A";
    private static final String username2 = "SI-B";
    private static final String password1 = "PASS-A";
    private static final String password2 = "PASS-B";
    
    // Data pengujian
    private static final String namaUji = "NAMA BARANG";
    private static final String lokasiUji = "LOKASI-XYZ";
    
    private static final String namaUji2 = "NAMA BARANG lain";
    private static final String lokasiUji2 = "LOKASI-ABC";    
}
