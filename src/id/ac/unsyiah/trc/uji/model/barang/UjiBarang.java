package id.ac.unsyiah.trc.uji.model.barang;

import org.junit.After;
import org.junit.Before; 
import org.junit.Test;

import static org.junit.Assert.*;

import id.ac.unsyiah.trc.model.barang.Barang;

/**
 * Unittest untuk menguji Entity Barang
 */
public class UjiBarang {
    @Before // Annotation ini untuk menandakan bahwa sebelum menjalankan satu 
    		// @Test jalankan method berikut ini terlebih dahulu
    public void setUp() { }
    
    @After // Annotation ini untuk menandakan bahwa sesudah menjalankan satu 
    	   // @Test jalankan method berikut ini
    public void tearDown() { }
    
    /**
     * Uji constructor.
     */
    @Test // Annotation ini untuk menandakan bahwa method berikut adalah satu 
    	  // unittest
    public void constructor() {
    	// Buat object-nya
    	Barang barang = new Barang(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertEquals(barang.getKode(), kodeUji);
    	assertEquals(barang.getNama(), namaUji);
    	assertEquals(barang.getKeterangan(), keteranganUji);
    }
    
    /**
     * Uji property kode
     */
    @Test
    public void kode() {
    	// Buat object-nya
    	Barang barang = new Barang(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertEquals(barang.getKode(), kodeUji);
    	
    	// Coba ubah property-nya
    	barang.setKode(kodeUji2);
    	// Uji benar apa tidak isinya
    	assertEquals(barang.getKode(), kodeUji2);
    }
    
    /**
     * Uji property nama
     */
    @Test
    public void nama() {
    	// Buat object-nya
    	Barang barang = new Barang(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertEquals(barang.getNama(), namaUji);
    	
    	// Coba ubah property-nya
    	barang.setNama(namaUji2);
    	// Uji benar apa tidak isinya
    	assertEquals(barang.getNama(), namaUji2);
    }
    
    /**
     * Uji property keterangan
     */
    @Test
    public void keterangan() {
    	// Buat object-nya
    	Barang barang = new Barang(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertEquals(barang.getKeterangan(), keteranganUji);
    	
    	// Coba ubah property-nya
    	barang.setKeterangan(keteranganUji2);
    	// Uji benar apa tidak isinya
    	assertEquals(barang.getKeterangan(), keteranganUji2);
    }
    
    // Data pengujian
    private static final String kodeUji = "KODE-XYZ";
    private static final String namaUji = "NAMA BARANG";
    private static final String keteranganUji = "KETERANGAN Barang";

    private static final String kodeUji2 = "KODE-ABC";
    private static final String namaUji2 = "NAMA BARANG lain";
    private static final String keteranganUji2 = "KETERANGAN Barang lain";    
}
