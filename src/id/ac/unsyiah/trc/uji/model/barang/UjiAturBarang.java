package id.ac.unsyiah.trc.uji.model.barang;

import org.junit.After;
import org.junit.Before; 
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.List;

import javax.jdo.JDOObjectNotFoundException;

import id.ac.unsyiah.trc.model.barang.Barang;
import id.ac.unsyiah.trc.model.barang.AturBarang;

// Package untuk Simulator GAE Datastore
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

/**
 * Unittest untuk menguji model.AturBarang
 *
 */
public class UjiAturBarang {
    @Before // Annotation ini untuk menandakan bahwa sebelum menjalankan satu 
    		// @Test jalankan method berikut ini terlebih dahulu
    public void setUp() {    	
        // Setup simulator datastore GAE 
        testHelper.setUp();
        
        aturBarang = new AturBarang();
    }
    
    @After // Annotation ini untuk menandakan bahwa sesudah menjalankan satu 
    	   // @Test jalankan method berikut ini
    public void tearDown() { 
    	// TearDown simulator datastore GAE
    	testHelper.tearDown();
    }
    
    /**
     * Uji model.AturBarang.baru
     */
    @Test
    public void baru() {
    	// Buat entity baru
    	Barang barang = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertNotNull(barang);
    	assertEquals(barang.getKode(), kodeUji);
    	assertEquals(barang.getNama(), namaUji);
    	assertEquals(barang.getKeterangan(), keteranganUji);
    	
    	// Uji benar tersimpan di datastore
    	Barang barangCari = aturBarang.cari(barang.getKey().getId());
    	// Uji benar apa tidak isinya
    	assertNotNull(barangCari);
    	assertEquals(barangCari.getKode(), kodeUji);
    	assertEquals(barangCari.getNama(), namaUji);
    	assertEquals(barangCari.getKeterangan(), keteranganUji);
    }
    
    /**
     * Uji model.AturBarang.ubah
     */
    @Test
    public void ubah() {
    	// Buat entity baru
    	Barang barang = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertNotNull(barang);
    	assertEquals(barang.getKode(), kodeUji);
    	assertEquals(barang.getNama(), namaUji);
    	assertEquals(barang.getKeterangan(), keteranganUji);
    	
    	// Ubah
    	aturBarang.ubah(barang.getKey().getId(), 
    					kodeUji2, 
    					namaUji2, 
    					keteranganUji2);

    	// Uji perubahan benar tersimpan di datastore
    	Barang barangCari = aturBarang.cari(barang.getKey().getId());
    	// Uji benar apa tidak isinya
    	assertNotNull(barangCari);
    	assertEquals(barangCari.getKode(), kodeUji2);
    	assertEquals(barangCari.getNama(), namaUji2);
    	assertEquals(barangCari.getKeterangan(), keteranganUji2);
    }
    
    /**
     * Uji model.AturBarang.ubah ID Salah
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void ubahIdSalah() {
    	// Ubah
    	aturBarang.ubah(-1, 
    					kodeUji2, 
    					namaUji2, 
    					keteranganUji2);
    }

    /**
     * Uji model.AturBarang.hapus
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void hapus() {
    	// Buat entity baru
    	Barang barang = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertNotNull(barang);
    	assertEquals(barang.getKode(), kodeUji);
    	assertEquals(barang.getNama(), namaUji);
    	assertEquals(barang.getKeterangan(), keteranganUji);
    	
    	// Hapus
    	Long id = barang.getKey().getId();
    	aturBarang.hapus(barang.getKey().getId());

    	// Uji penghapusan benar terjadi di datastore
    	aturBarang.cari(id); // Akan gagal dan throw exception
    }
    
    /**
     * Uji model.AturBarang.hapus id salah
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void hapusIdSalah() {
    	aturBarang.hapus(-1);
    }

    /**
     * Uji model.AturBarang.cari
     */
    @Test
    public void cari() {
    	// Buat entity baru
    	Barang barang = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertNotNull(barang);
    	assertEquals(barang.getKode(), kodeUji);
    	assertEquals(barang.getNama(), namaUji);
    	assertEquals(barang.getKeterangan(), keteranganUji);
    	
    	// Uji benar tersimpan di datastore
    	Barang barangCari = aturBarang.cari(barang.getKey().getId());
    	// Uji benar ketemu
    	assertNotNull(barangCari);
    	assertNotNull(barangCari.getKey());
    	assertEquals(barangCari.getKey().getId(), barang.getKey().getId());
    }
    
    /**
     * Uji model.AturBarang.cari id salah
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void cariIdSalah() {
    	// Uji benar tersimpan di datastore
    	aturBarang.cari(-1);
    }
    
    /**
     * Uji model.AturBarang.daftar kosong
     */
    @Test
    public void daftar0() {
    	// Ambil daftar barang
    	List<Barang> daftarBarang = aturBarang.daftar();
    	
    	// Uji kosong
    	assertNotNull(daftarBarang);
    	assertEquals(daftarBarang.size(), 0);
    }
    
    /**
     * Uji model.AturBarang.daftar berisi 1
     */
    @Test
    public void daftar1() {
    	// Buat entity baru
    	Barang barang = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	
    	// Ambil daftar barang
    	List<Barang> daftarBarang = aturBarang.daftar();
    	
    	// Uji berisi 1
    	assertNotNull(daftarBarang);
    	assertEquals(daftarBarang.size(), 1);
    	assertNotNull(daftarBarang.get(0));
    	assertEquals(daftarBarang.get(0).getKey().getId(), 
    				 barang.getKey().getId());
    }
    
    /**
     * Uji model.AturBarang.daftar berisi 2
     */
    @Test
    public void daftar2() {
    	// Buat entity baru
    	Barang barang1 = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	Barang barang2 = aturBarang.baru(kodeUji2, namaUji2, keteranganUji2);
    	
    	// Ambil daftar barang
    	List<Barang> daftarBarang = aturBarang.daftar();
    	
    	// Uji berisi 2
    	assertNotNull(daftarBarang);
    	assertEquals(daftarBarang.size(), 2);
    	assertNotNull(daftarBarang.get(0));
    	assertEquals(daftarBarang.get(0).getKey().getId(), 
    				 barang1.getKey().getId());
    	assertNotNull(daftarBarang.get(1));
    	assertEquals(daftarBarang.get(1).getKey().getId(), 
    				 barang2.getKey().getId());
    }
    
    /**
     * Uji model.AturBarang.daftar berisi 3
     */
    @Test
    public void daftar3() {
    	// Buat entity baru
    	Barang barang1 = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	Barang barang2 = aturBarang.baru(kodeUji2, namaUji2, keteranganUji2);
    	Barang barang3 = aturBarang.baru(kodeUji3, namaUji3, keteranganUji3);
    	
    	// Ambil daftar barang
    	List<Barang> daftarBarang = aturBarang.daftar();
    	
    	// Uji berisi 2
    	assertNotNull(daftarBarang);
    	assertEquals(daftarBarang.size(), 3);
    	assertNotNull(daftarBarang.get(0));
    	assertEquals(daftarBarang.get(0).getKey().getId(), 
    				 barang1.getKey().getId());
    	assertNotNull(daftarBarang.get(1));
    	assertEquals(daftarBarang.get(1).getKey().getId(), 
    				 barang2.getKey().getId());
    	assertNotNull(daftarBarang.get(2));
    	assertEquals(daftarBarang.get(2).getKey().getId(), 
    				 barang3.getKey().getId());
    }
    
    /**
     * Uji model.AturBarang.cari berdasarkan kode
     */
    @Test
    public void cariKode() {
    	// Buat entity baru
    	Barang barang = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertNotNull(barang);
    	assertEquals(barang.getKode(), kodeUji);
    	assertEquals(barang.getNama(), namaUji);
    	assertEquals(barang.getKeterangan(), keteranganUji);
    	
    	// Uji benar tersimpan di datastore
    	Barang barangCari = aturBarang.cari(kodeUji);
    	// Uji benar ketemu
    	assertNotNull(barangCari);
    	assertNotNull(barangCari.getKey());
    	assertEquals(barangCari.getKey().getId(), barang.getKey().getId());
    }
    
    /**
     * Uji model.AturBarang.cari berdasarkan kode Salah kode
     */
    @Test
    public void cariKodeSalahKode() {
    	// Buat entity baru
    	Barang barang = aturBarang.baru(kodeUji, namaUji, keteranganUji);
    	
    	// Uji benar apa tidak isinya
    	assertNotNull(barang);
    	assertEquals(barang.getKode(), kodeUji);
    	assertEquals(barang.getNama(), namaUji);
    	assertEquals(barang.getKeterangan(), keteranganUji);
    	
    	// Uji benar tersimpan di datastore
    	Barang barangCari = aturBarang.cari("-1");
    	// Uji benar tidak ketemu
    	assertNull(barangCari);
    }
    
    private static final String kodeUji = "KODE-XYZ";
    private static final String namaUji = "NAMA BARANG";
    private static final String keteranganUji = "KETERANGAN Barang";

    private static final String kodeUji2 = "KODE-ABC";
    private static final String namaUji2 = "NAMA BARANG lain";
    private static final String keteranganUji2 = "KETERANGAN Barang lain";

    private static final String kodeUji3 = "KODE-ABC-3";
    private static final String namaUji3 = "NAMA BARANG lain 3";
    private static final String keteranganUji3 = "KETERANGAN Barang lain 3";

    private AturBarang aturBarang;
    
    // Package untuk Simulator GAE Datastore
    private final LocalServiceTestHelper testHelper = 
    		  new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());    
}