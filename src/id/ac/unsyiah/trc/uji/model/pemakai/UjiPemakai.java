/**
 * Unit test untuk model.Pemakai
 */
package id.ac.unsyiah.trc.uji.model.pemakai;

import static org.junit.Assert.assertEquals;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Unittest untuk model.pemakai.Pemakai
 */
public class UjiPemakai {
	/**
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	@Before
    public void setUp() 
    			throws NoSuchAlgorithmException, InvalidKeySpecException {
		// Untuk GAE
		testHelper.setUp();
		
		// Setup semua
		pemakai = new Pemakai(username, password, salt, peran);
	}
	
    @After
    public void tearDown() {
		// Untuk GAE
		testHelper.tearDown(); 
    }
	
    @Test
	public void testGetter() {
		assertEquals(pemakai.getUsername(), username);
		assertEquals(pemakai.getPassword() != null, true);
		assertEquals(pemakai.getSalt() != null, true);
		assertEquals((long) pemakai.getPeran(), peran);
		assertEquals(pemakai.getKey(), null);
	}

    /**
     * 
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Test
	public void testSetter() 
				throws NoSuchAlgorithmException, InvalidKeySpecException {
    	// Nilai Baru 
        long peranBaru = Pemakai.PERAN_BUKAN_ADMIN;
        byte[] salt = {0x12, 0x34, 0x56}; //test byte salt 
        byte[] passwordBaruEnkripsi = {0x76, 0x54, 0x32}; //test password

        // Set nilai baru 
		pemakai.setPassword(passwordBaruEnkripsi);
		pemakai.setSalt(salt);
		pemakai.setPeran(peranBaru);
		
		// Check apa benar
		assertEquals(pemakai.getPassword().length, passwordBaruEnkripsi.length);
		for (int cnt=0; cnt<passwordBaruEnkripsi.length; ++cnt)
			assertEquals(pemakai.getPassword()[cnt], passwordBaruEnkripsi[cnt]);
		assertEquals(pemakai.getSalt().length, salt.length);
		for (int cnt=0; cnt<salt.length; ++cnt)
			assertEquals(pemakai.getSalt()[cnt], salt[cnt]);
		assertEquals((long) pemakai.getPeran(), peranBaru);
		assertEquals(pemakai.getKey(), null);
	}

	private final LocalServiceTestHelper testHelper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    private Pemakai pemakai = null;
    
    private String username = "a@a.a";
    private byte[] password = {0x12, 0x34, 0x56};
    private byte[] salt = {0x65, 0x43, 0x21};
    private long peran = Pemakai.PERAN_ADMIN;
}