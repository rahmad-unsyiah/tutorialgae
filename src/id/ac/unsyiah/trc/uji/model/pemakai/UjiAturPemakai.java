/**
 * Unit test untuk model.AturPemakai
 */
package id.ac.unsyiah.trc.uji.model.pemakai;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.jdo.JDOObjectNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import id.ac.unsyiah.trc.eksepsi.DuplikasiUsername;
import id.ac.unsyiah.trc.eksepsi.LoginSalah;
import id.ac.unsyiah.trc.model.pemakai.AturPemakai;
import id.ac.unsyiah.trc.model.pemakai.Pemakai;

/**
 * Unittest untuk model.pemakai.AturPemakai
 */
public class UjiAturPemakai {
	/**
	 * Setup unittest 
	 */
    @Before
    public void setUp() 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Untuk GAE
        testHelper.setUp();

        ctrl = new AturPemakai();
        
		// Buat Pemakai baru
        pemakai = ctrl.baru(username, password, peran);
    }

    /**
     * Bereskan setelah unittest dijalankan
     */
    @After
    public void tearDown() {       
        // Hapus Pemakai yang dibuat
        if (pemakai != null)
            ctrl.hapus(pemakai.getKey().getId());

        // Untuk GAE
        testHelper.tearDown(); 
    }

    /**
     * Tambah dan hapus satu entitas
     */
    @Test
    public void testBaruDanHapus() 
                throws NoSuchAlgorithmException, 
                	   InvalidKeySpecException, 
                	   UnsupportedEncodingException {
    	String usernameBaru = "c@c.c";
        String passwordBaru = "Pass2";
        long peranBaru = Pemakai.PERAN_BUKAN_ADMIN;
        
        // Baru
        Pemakai pemakaiBaru = ctrl.baru(usernameBaru, passwordBaru, peranBaru);
        // Periksa
        assertEquals(pemakaiBaru.getUsername(), usernameBaru);
        assertEquals(pemakaiBaru.getPeran(), peranBaru);
        assertEquals(pemakaiBaru.getKey() != null, true);
    }
    
    /*
     * Unit test untuk menambah pemakai baru dengan username yang sudah 
     * terdaftar
     * 
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Test(expected=DuplikasiUsername.class)
    public void testBaruUsernameSudahAda() 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        String passwordBaru = "Pass2";
        long peranBaru = Pemakai.PERAN_BUKAN_ADMIN;
        
        ctrl.baru(username, passwordBaru, peranBaru);
    }

    /**
     * Hapus satu entitas yang ID nya tidak dikenal
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void testHapusPemakaiTidakAda() {
        ctrl.hapus(-1l);
    }

    /**
     * Cari satu entitas
     */
    @Test
    public void testCariPemakaiDenganIDYangAda() {
        // cari yg ada 
        Pemakai pemakaiCari = ctrl.cari(pemakai.getKey().getId());
        // Periksa      
        assertEquals(pemakaiCari.getUsername(), username);
        assertEquals(pemakaiCari.getPeran(), peran);
        assertEquals(pemakaiCari.getKey(), pemakai.getKey());
    }
    
    /**
     * Cari satu entitas yang ID-nya tidak ada
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void testCariPemakaiDenganIDYangTidakAda() {
        // Cari yg tidak ada 
        ctrl.cari(-1l);
    }
    
    /**
     * Ubah password Pemakai yang ID nya tidak ada
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void testUbahPasswordPemakaiYangTidakAda() 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Ubah
        ctrl.ubahPassword(-1l, "...");
    }
    
    /**
     * ubah password
     * 
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Test
    public void testUbahPassword() 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        String passwordBaru = "passBaru";
        
        // Ubah 
        ctrl.ubahPassword(pemakai.getKey().getId(), passwordBaru);
        
        // Cari dan periksa apa benar
        Pemakai pemakaiCari = ctrl.cari(pemakai.getKey().getId());
        assertEquals(pemakaiCari.getUsername(), username);
        assertEquals(pemakaiCari.getPeran(), peran);
        assertEquals(pemakaiCari.getKey(), pemakai.getKey());
    }

    /**
     * Ubah peran pemakai yang ID-nya tidak ada
     */
    @Test(expected=JDOObjectNotFoundException.class)
    public void testUbahPeranPemakaiYangTidakAda() {
        // Ubah
        ctrl.ubahPeran(-1l, Pemakai.PERAN_BUKAN_ADMIN);
    }
    
    /**
     * Ubah peran sebuah Pemakai
     */
    @Test
    public void testUbahPeran() {
        long peranBaru = Pemakai.PERAN_BUKAN_ADMIN;
        
        // Ubah 
        ctrl.ubahPeran(pemakai.getKey().getId(), peranBaru);
        
        // Cari dan periksa apa benar
        Pemakai pemakaiCari = ctrl.cari(pemakai.getKey().getId());
        assertEquals(pemakaiCari.getUsername(), username);
        assertEquals(pemakaiCari.getPeran(), peranBaru);
        assertEquals(pemakaiCari.getKey(), pemakai.getKey());
    }

    /**
     * Ambil daftar pemakai kosong
     */
    @Test
    public void testDaftarTanpaHasil() 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        ctrl.hapus(pemakai.getKey().getId());
        
        // Ambil daftar
        List<Pemakai> daftar = ctrl.daftar();
        assertEquals(daftar.size(), 0);
        
        // Buat lagi pemakai agar bisa dihapus oleh breakdown
        pemakai = ctrl.baru(username, password, peran);
    }
    
    /**
     * Ambil daftar pemakai yang berisi satu pemakai 
     */
    @Test
    public void testDaftarHasilSatu() 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Ambil daftar
        List<Pemakai> daftar = ctrl.daftar();
        assertEquals(daftar.size(), 1);
        assertEquals(daftar.get(0).getUsername(), this.username);
        assertEquals(daftar.get(0).getPeran(), this.peran);
    }
 
    /**
     * Ambil daftar pemakai yang kembalikan dua pemakai 
     */
    @Test
    public void testDaftarAdminHasilDua() 
                throws NoSuchAlgorithmException, InvalidKeySpecException {
        String username1 = "x@y.z";
        String password1 = "Pass1";
        long peran1 = Pemakai.PERAN_ADMIN;
        
        // Baru
        Pemakai pemakai1 = ctrl.baru(username1, password1, peran1);
        // Periksa
        assertEquals(pemakai1.getUsername(), username1);
        assertEquals(pemakai1.getPeran(), peran1);
        assertEquals(pemakai1.getKey() != null, true);
        
        // Ambil daftar
        List<Pemakai> daftar = ctrl.daftar();
        assertEquals(daftar.size(), 2);
        assertEquals(daftar.get(0).getUsername(), this.username);
        assertEquals(daftar.get(0).getPeran(), this.peran);
        
        // Hapus
        ctrl.hapus(pemakai1.getKey().getId());
    }

    /**
     * Cari pemakai berdasarkan Username yang tidak dikenal
     */
    @Test
    public void testCariUsernameHasilNol() {
        Pemakai hasil = ctrl.cariUsername("...");
        assertEquals(hasil, null);
    }

    /**
     * Cari pemakai berdasarkan Username yang menghasilkan satu pemakai
     */
    @Test
    public void testCariUsernameHasilSatu() {
        // Cari
        Pemakai hasil = ctrl.cariUsername(this.username);
        assertEquals(hasil != null, true);
        
        // Periksa
        assertEquals(hasil.getUsername(), this.username);
        assertEquals(hasil.getPeran(), this.peran);
        assertEquals(hasil.getKey() != null, true);
    }

    /**
     * Unittest periksaLogin dengan Username dan password benar.
     * @throws InvalidKeySpecException 
     * @throws NoSuchAlgorithmException 
     * @throws LoginSalah 
     */
    @Test
    public void periksaLogin() 
    			throws LoginSalah, 
    				   NoSuchAlgorithmException, 
    				   InvalidKeySpecException {
    	Pemakai pemakaiPeriksa = new AturPemakai().periksaLogin(username, 
    															password);
    	
    	// Periksa
    	assertNotNull(pemakaiPeriksa);
    	assertNotNull(pemakaiPeriksa.getKey());
    	assertEquals(pemakaiPeriksa.getKey().getId(), pemakai.getKey().getId());
    }
    
    /**
     * Unittest periksaLogin dengan Username salah
     * @throws InvalidKeySpecException 
     * @throws NoSuchAlgorithmException 
     * @throws LoginSalah 
     */
    @Test(expected=LoginSalah.class)
    public void periksaUsernameSalah() 
    			throws LoginSalah, 
    			       NoSuchAlgorithmException, 
    			       InvalidKeySpecException {
    	new AturPemakai().periksaLogin("xxx", password);
    }
    
    /**
     * Unittest periksaLogin dengan password salah.
     */
    @Test(expected=LoginSalah.class)
    public void periksaPasswordSalah()  
				throws LoginSalah, 
					  NoSuchAlgorithmException, 
					  InvalidKeySpecException { 
    	new AturPemakai().periksaLogin(username, "xxx");
    }
    
    private final LocalServiceTestHelper testHelper = 
    		new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    AturPemakai ctrl = null;
    Pemakai pemakai = null;
    
    // Data pengujian
    private String username = "a@b.c";
    private String password = "MyPass";
    private long peran = Pemakai.PERAN_ADMIN;
}